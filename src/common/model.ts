export enum FilterType {
    Number, // a single number
    NumberRange, // two numbers that form a range
    SelectOne, // a single option from a drop-down box
    SelectMany, // any number of selections from a drop-down box
}

export interface FilterValue {
    filterId: string // ID (short name) of the filter
    values: number[] // Values set for this filter (usually just 1, except for certain fitler types)
}

export interface FilterInfo {
    id: string // short name, used for serialization
    name: string // display name of the filter (e.g. 'game mode', 'filter by year', etc)
    prompt: string // prompt text, only applies to selection-type filters (e.g. 'original release date')
    type: FilterType // type of the filter (e.g. enter a number, select from a dropdown)
    names: string[] // for selection-type filters, get the list of dropdown entries
    defaultValues: number[] // default values (number of array elems depending on types)
}

export interface ProviderInfo {
    id: string // short name, used for serialization
    name: string // display name, shown in the GUI
    description: string[] // lines of HTML that form a description of the provider
    title: string // HTML title string (e.g. "Gamershots: Name That Screenshot!")
    prompt: string // Prompt for the popup box (e.g. "What game is this?")
    elemDetailsTitle: string // Title of the element details popup (e.g. "Game Info")
    filters: FilterInfo[] // All filters that can be set by the user
}

export interface FilterPreset {
    name: string
    providerId: string
    filterValues: FilterValue[]
}

export interface FilterPresets {
    presets: FilterPreset[],
}

export interface ProviderReport {
    providers: ProviderInfo[]
}

export interface Player {
    name: string
    providerId: string
    filterValues: FilterValue[]

    // scores for the category chosen by this player
    perPlayerScores: number[]
    incorrectGuesses: number
    everybodyDrinksCount: number
}

export interface Game {
    players: Player[]
    currentPlayerIndex: number
    numTurns: number
    currentTurn: number
}

export interface ContentQuery {
    providerId: string
    filterValues: FilterValue[]
}

export interface FoundContent {
    result: 'FOUND'
    content: string
    solution: string
    detailsUrl: string
}

export interface NotFoundContent {
    result: 'NOT_FOUND'
}

export type Content = FoundContent | NotFoundContent

export interface FilterTestResult {
    filteredCount: number | 'UNKNOWN'
    totalCount: number | 'UNKNOWN'
    approximate?: boolean
}
