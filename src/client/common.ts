import * as model from '../common/model.js'

const apiRoot = (window.location.hostname === '127.0.0.1')
    ? 'http://127.0.0.1:3000' // for local testing
    : 'https://ya922bdgng.execute-api.us-east-1.amazonaws.com/prod'

export enum Screen {
    Empty,
    Splash,
    Configuration,
    Presets,
    Content,
    Stats,
    About,
}

export interface GameRunner {
    showLoading(text: string): void
    hideLoading(): void
    showError(error: any): void
    renderOverlays(): JSX.Element[]
    renderToolbar(activeScreen: Screen, extraClass?: string): JSX.Element
    findProvider(id: string): model.ProviderInfo | undefined
    getProviders(): model.ProviderReport
    getPresets(): model.FilterPresets
    setPresets(presets: model.FilterPresets): Promise<void>
}

export function goToHash(hashLocation: string) {
    if (window.location.hash === `#${hashLocation}`) {
        window.dispatchEvent(new HashChangeEvent('hashchange'))
    } else {
        window.location.href = `#${hashLocation}`
    }
}

export function get<T>(path: string): Promise<T> {
    return new Promise<T>((resolve, reject) => {
        $.get(`${apiRoot}/${path}`)
            .fail((err: any) => reject(err))
            .done((data: any) => resolve(data as T))
    })
}

export function post<T>(path: string, data: object): Promise<T> {
    return new Promise<T>((resolve, reject) => {
        $.post(`${apiRoot}/${path}`, JSON.stringify(data))
            .fail((err: any) => reject(err))
            .done((resultData: any) => resolve(resultData as T))
    })
}

export function getCookie(name: string): string {
    name += '='
    for (let c of document.cookie.split(';')) {
        while (c.charAt(0) === ' ') {
            c = c.substring(1)
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length)
        }
    }
    return ''
}

export function setCookie(name: string, val: string) {
    document.cookie = `${name}=${val}; expires=Fri, 31 Dec 9999 23:59:59 GMT`
}

export function getSavedGame(): model.Game | undefined {
    const serializedGame = getCookie('omnishots-game')
    if (serializedGame) {
        const game: model.Game = JSON.parse(atob(serializedGame))
        if (game.players && game.players.length) {
            return game
        }
    }
    return undefined
}

export function saveGame(game: model.Game) {
    setCookie('omnishots-game', btoa(JSON.stringify(game)))
}

export function deepCopy<T>(val: T): T {
    return JSON.parse(JSON.stringify(val))
}
