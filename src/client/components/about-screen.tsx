import * as React from 'react'
import * as common from '../common.js'

export interface AboutScreenProps {
    runner: common.GameRunner
}

export class AboutScreenComponent extends React.Component<AboutScreenProps, unknown> {
    public render() {
        return <div className='omnishots-main column'>
            {this.props.runner.renderOverlays()}
            <h2 className='ui top attached header'>
                omnishots
            </h2>
            <div className='ui left aligned bottom attached segment'>
                <p>omnishots is a drinking game for nerds.</p>
                <p>TODO: details / donate link???</p>
                <button
                    className='positive ui button'
                    onClick={() => common.goToHash('play') }>
                    Back To The Game
                </button>
            </div>
        </div>
    }
}
