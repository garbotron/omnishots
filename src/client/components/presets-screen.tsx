import * as React from 'react'
import * as model from '../../common/model.js'
import * as common from '../common.js'
import { PlayerDetailsEditorComponent } from './player-details-editor.js'

export interface PresetsScreenProps {
    runner: common.GameRunner
    game: model.Game
}

export interface PresetsScreenState {
    presets: model.FilterPresets
    resultCounts: (model.FilterTestResult | undefined)[]
    isEditing: boolean
    editPresetIndex: number
    editPresetProviderId: string
    editPresetFilterValues: model.FilterValue[]
    newPresetName: string
    newPresetProviderId: string
}

export class PresetsScreenComponent extends React.Component<PresetsScreenProps, PresetsScreenState> {
    private newPresetModal: HTMLDivElement | undefined = undefined
    private newPresetForm: HTMLDivElement | undefined = undefined
    private newPresetProviderDropdown: HTMLDivElement | undefined = undefined

    constructor(props: PresetsScreenProps) {
        super(props)
        this.state = {
            presets: { presets: [] },
            resultCounts: [],
            isEditing: false,
            editPresetIndex: -1,
            editPresetProviderId: '',
            editPresetFilterValues: [],
            newPresetName: '',
            newPresetProviderId: '',
        }
    }

    public componentWillMount() {
        this.setState({ presets: common.deepCopy(this.props.runner.getPresets()) })
    }

    public componentDidMount() {
        this.setupElements()
        for (let i = 0; i < this.state.presets.presets.length; i++) {
            this.updateResultCount(i)
        }
    }

    public componentDidUpdate() {
        this.setupElements()
    }

    public render() {
        let mainDiv: JSX.Element
        if (this.state.isEditing) {
            const preset = this.state.presets.presets[this.state.editPresetIndex]
            mainDiv = (
                <div className='ui left aligned segment'>
                    <PlayerDetailsEditorComponent
                        runner={this.props.runner}
                        initialProviderId={preset.providerId}
                        initialFilterValues={preset.filterValues}
                        providerChanged={(x) => this.editProviderIdChanged(x)}
                        filterValuesChanged={(x) => this.editPresetValuesChanged(x)} />
                    <div className='ui basic right aligned segment' style={{ padding: 0 }}>
                        <button
                            className='ui primary button'
                            style={{ marginTop: '1em' }}
                            onClick={() => this.presetEditApply()}>Apply Changes</button>
                        <button className='ui button' onClick={() => this.presetEditDiscard()}>Discard Changes</button>
                    </div>
                </div>
            )
        } else {
            const providers = this.props.runner.getProviders()
            const providerItems = providers.providers.map((x) =>
                <div key={x.id} data-value={x.id} className='item'>{x.name}</div>)

            const newPresetModal = (
                <div className='ui small modal' ref={(x) => this.newPresetModal = x || undefined}>
                    <div className='header'>Add New Preset</div>
                    <div className='content'>
                        <div className='ui form' ref={(x) => this.newPresetForm = x || undefined}>
                            <div className='field'>
                                <label>Preset Name</label>
                                <input
                                    type='text'
                                    name='name'
                                    placeholder='Preset Name'
                                    onChange={(e: any) => this.setNewPresetName(e.target.value)}>
                                </input>
                            </div>
                            <div className='field'>
                                <label>Game Type</label>
                                <div className='ui selection dropdown'
                                    ref={(x) => this.newPresetProviderDropdown = x || undefined}>
                                    <input name='provider' type='hidden' value={this.state.newPresetProviderId} />
                                    <i className='dropdown icon'></i>
                                    <div className='default text'>Select Game Type</div>
                                    <div className='menu'>{providerItems}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='actions'>
                        <button className='ui positive button'>Configure {this.state.newPresetName}</button>
                        <button className='ui deny button'>Cancel</button>
                    </div>
                </div>
            )

            const presetRows = this.state.presets.presets.map((x, i) =>
                <tr key={i}>
                    <td className='ui small header'>{x.name}</td>
                    <td>{this.getProviderSummary(x)}</td>
                    {this.renderResultCountCell(i)}
                    <td className='right aligned collapsing selectable'>
                        <a href='#' title='Edit Configuration'
                            onClick={(e) => {
                                e.preventDefault()
                                this.editPresetIndex(i)
                            }}>
                            <i className='large edit icon'></i>
                        </a>
                    </td>
                    <td className='right aligned collapsing negative selectable'>
                        <a href='#' title='Delete Preset'
                            onClick={(e) => {
                                e.preventDefault()
                                this.deletePresetIndex(i)
                            }}>
                            <i className='large delete icon'></i>
                        </a>
                    </td>
                </tr>,
            )

            mainDiv = (
                <div className='ui left aligned clearing segment'>
                    <div className='ui menu'>
                        <div className='right menu'>
                            {newPresetModal}
                            <a className='item' onClick={() => this.openNewPresetModal()}>
                                <i className='green filter icon'></i>Add Preset
                            </a>
                        </div>
                    </div>
                    <table className='ui very compact single line celled table'>
                        <thead>
                            <tr>
                                <th>Preset Name</th>
                                <th>Game Type</th>
                                <th>Filter Result</th>
                                <th className='collapsing'></th>
                                <th className='collapsing'></th>
                            </tr>
                        </thead>
                        <tbody>{presetRows}</tbody>
                    </table>

                    <button
                        className='ui right floated button'
                        onClick={() => this.discardChanges()}>
                        Discard Changes
                    </button>
                    <button
                        className='ui right floated primary button'
                        onClick={() => this.commitChanges()}>
                        Save Changes
                    </button>
                </div>
            )
        }

        return <div className='omnishots-main column'>
            {this.props.runner.renderOverlays()}
            {this.props.runner.renderToolbar(common.Screen.Configuration)}
            {mainDiv}
        </div>
    }

    private setupElements() {
        $(this.newPresetForm!).form({ fields: { name: 'empty', provider: 'empty' } })
        $(this.newPresetProviderDropdown!).dropdown({
            onChange: (x: string) => this.setState({ newPresetProviderId: x }),
        })
    }

    private renderResultCountCell(presetIndex: number) {
        if (presetIndex >= this.state.resultCounts.length) {
            return <td><div className='ui active tiny inline loader'></div></td>
        }
        const count = this.state.resultCounts[presetIndex]
        const fmt = Intl.NumberFormat(undefined, { maximumFractionDigits: 1 })
        if (!count) {
            return <td><div className='ui active tiny inline loader'></div></td>
        } else if (count.filteredCount === 'UNKNOWN') {
            return <td className='disabled'>unknown</td>
        } else if (count.totalCount === 'UNKNOWN') {
            return <td>{count.approximate ? '~' : ''}{fmt.format(count.filteredCount)}</td>
        } else {
            const num = fmt.format(count.filteredCount)
            const percent = fmt.format((count.filteredCount * 100) / count.totalCount)
            return <td>{count.approximate ? '~' : ''}{num} ({percent}%)</td>
        }
    }

    private presetEditApply() {
        const preset = this.state.presets.presets[this.state.editPresetIndex]
        preset.providerId = this.state.editPresetProviderId
        preset.filterValues = this.state.editPresetFilterValues
        const resultCounts = common.deepCopy(this.state.resultCounts)
        resultCounts[this.state.editPresetIndex] = undefined
        this.setState({ resultCounts, isEditing: false })
        this.updateResultCount(this.state.editPresetIndex)
    }

    private presetEditDiscard() {
        this.setState({ isEditing: false })
    }

    private openNewPresetModal() {
        $(this.newPresetModal!).modal({
            onApprove: () => {
                if (!$(this.newPresetForm!).form('is valid')) {
                    return false
                }
                this.addNewPreset(this.state.newPresetName, this.state.newPresetProviderId)
                return
            },
        }).modal('show')
    }

    private setNewPresetName(name: string) {
        this.setState({ newPresetName: name })
    }

    private addNewPreset(name: string, providerId: string) {
        const provider = this.props.runner.findProvider(providerId)
        if (!provider) {
            this.props.runner.showError('bad provider')
            return
        }
        const presets = common.deepCopy(this.state.presets)
        presets.presets.push({ name, providerId, filterValues: [] })

        // open editor unless there are no filters
        if (provider.filters.length > 0) {
            this.setState({
                presets,
                isEditing: true,
                editPresetIndex: presets.presets.length - 1,
                editPresetProviderId: providerId,
                editPresetFilterValues: [],
                newPresetName: '',
                newPresetProviderId: '',
            })
        } else {
            this.setState({
                presets,
                newPresetName: '',
                newPresetProviderId: '',
            })
            this.updateResultCount(presets.presets.length - 1)
        }
    }

    private editPresetIndex(index: number) {
        this.setState({
            isEditing: true,
            editPresetIndex: index,
            editPresetProviderId: this.state.presets.presets[index].providerId,
            editPresetFilterValues: this.state.presets.presets[index].filterValues.map(common.deepCopy),
        })
    }

    private deletePresetIndex(index: number) {
        const presets = common.deepCopy(this.state.presets)
        presets.presets.splice(index, 1)
        this.setState({ presets })
    }

    private editProviderIdChanged(providerId: string) {
        this.setState({ editPresetProviderId: providerId })
    }

    private editPresetValuesChanged(values: model.FilterValue[]) {
        this.setState({ editPresetFilterValues: values })
    }

    private discardChanges() {
        common.goToHash('config')
    }

    private commitChanges() {
        this.props.runner.showLoading('updating presets')
        this.props.runner.setPresets(this.state.presets).then(() => {
            this.props.runner.hideLoading()
            common.goToHash('config')
        })
    }

    private getProviderSummary(preset: model.FilterPreset) {
        const provider = this.props.runner.findProvider(preset.providerId)
        if (!provider) {
            return 'Unknown'
        } else if (preset.filterValues.length === 0) {
            return provider.name
        } else if (preset.filterValues.length === 1) {
            return `${provider.name} - 1 setting`
        } else {
            return `${provider.name} - ${preset.filterValues.length} settings`
        }
    }

    private updateResultCount(playerIndex: number) {
        const preset = this.state.presets.presets[playerIndex]
        const input: model.ContentQuery = {
            providerId: preset.providerId,
            filterValues: preset.filterValues,
        }
        common.post<model.FilterTestResult>('test', input)
            .then((result) => {
                const resultCounts = common.deepCopy(this.state.resultCounts)
                while (playerIndex >= resultCounts.length) {
                    resultCounts.push({ filteredCount: 'UNKNOWN', totalCount: 'UNKNOWN' })
                }
                resultCounts[playerIndex] = result
                this.setState({ resultCounts })
            })
            .catch((err) => this.props.runner.showError(err))
    }
}
