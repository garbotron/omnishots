import * as React from 'react'
import * as model from '../../common/model.js'
import * as common from '../common.js'

export interface StatsScreenProps {
    runner: common.GameRunner
    game: model.Game
}

export interface StatsScreenState {
    showingPlayerStats: boolean
}

interface PlayerStatsInfo {
    player: model.Player
    correct: number
    incorrect: number
    steals: number
    totalScore: number
}

interface CategoryStatsInfo {
    player: model.Player
    gameType: string
    correct: string
    incorrect: string
    steals: string
    everybodyDrinks: string
}

export class StatsScreenComponent extends React.Component<StatsScreenProps, StatsScreenState> {
    constructor(props: StatsScreenProps) {
        super(props)
        this.state = { showingPlayerStats: true }
    }

    public render() {
        const playerStats = this.props.game.players.map((x, i) => this.getPlayerStatsInfo(x, i))
        // sort player stats by total score, decending
        const playerStatsSorted = playerStats.sort((x, y) => {
            if (x.totalScore === y.totalScore) {
                return x.player.name.localeCompare(y.player.name)
            } else {
                return y.totalScore - x.totalScore
            }
        })

        let statsTable: JSX.Element
        if (this.state.showingPlayerStats) {
            statsTable = (
                <table className='ui celled definition table'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Correct</th>
                            <th>Incorrect</th>
                            <th>Steals</th>
                            <th>Total Points</th>
                        </tr>
                    </thead>
                    <tbody>
                        {playerStatsSorted.map((x) => this.renderPlayerStatsRow(x))}
                    </tbody>
                </table>
            )
        } else {
            const categoryStats = this.props.game.players.map((x) => this.getCategoryStatsInfo(x))
            statsTable = (
                <table className='ui celled definition table'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Correct</th>
                            <th>Incorrect</th>
                            <th>Steals</th>
                            <th>Everybody Drinks</th>
                        </tr>
                    </thead>
                    <tbody>
                        {categoryStats.map((x) => this.renderCategoryStatsRow(x))}
                    </tbody>
                </table>
            )
        }

        const isGameOver = this.props.game.numTurns !== 0 && this.props.game.currentTurn >= this.props.game.numTurns
        let winner: JSX.Element | undefined
        if (isGameOver && playerStatsSorted.length > 0) {
            winner = <h2 className='ui green header'>Winner: {playerStatsSorted[0].player.name}</h2>
        }

        let gameOverButtons: JSX.Element | undefined
        if (isGameOver) {
            gameOverButtons = <div className='ui basic segment' style={{ padding: 0 }}>
                <div className='ui button' onClick={() => this.newGame()}>New Game</div>
                <div className='ui button' onClick={() => this.continueGame()}>Continue This Game (same players)</div>
                <div className='ui button' onClick={() => this.reconfigureGame()}>Reconfigure Game</div>
            </div>
        }

        const mainDiv = (
            <div className='ui segment'>
                {winner}
                <div className='ui massive secondary pointing menu'>
                    <a className={this.state.showingPlayerStats ? 'active item' : 'item'}
                        onClick={() => this.setState({ showingPlayerStats: true})}>
                        <i className='user icon'></i>
                        Player Stats
                    </a>
                    <div className='right menu'>
                        <a className={this.state.showingPlayerStats ? 'item' : 'active item'}
                            onClick={() => this.setState({ showingPlayerStats: false})}>
                            <i className='list layout icon'></i>
                            Category Stats
                        </a>
                    </div>
                </div>
                {statsTable}
                {gameOverButtons}
            </div>
        )
        return <div className='omnishots-main column'>
            {this.props.runner.renderOverlays()}
            {this.props.runner.renderToolbar(common.Screen.Stats)}
            {mainDiv}
        </div>
    }

    private getPlayerStatsInfo(player: model.Player, playerIdx: number): PlayerStatsInfo {
        const ret = { player, correct: 0, incorrect: 0, steals: 0, totalScore: 0 }

        // loop over each player's category (including the current player) and tally up the scores
        this.props.game.players.forEach((p) => {
            ret.totalScore += p.perPlayerScores[playerIdx] || 0
            if (p === player) {
                ret.correct += p.perPlayerScores[playerIdx] || 0
                ret.incorrect += p.incorrectGuesses
            } else {
                ret.steals += p.perPlayerScores[playerIdx] || 0
            }
        })

        return ret
    }

    private getCategoryStatsInfo(player: model.Player): CategoryStatsInfo {
        let correct = 0
        let steals = 0

        this.props.game.players.forEach((p, i) => {
            if (p === player) {
                correct += p.perPlayerScores[i] || 0
            } else {
                steals += p.perPlayerScores[i] || 0
            }
        })

        const total = correct + player.incorrectGuesses + steals + player.everybodyDrinksCount
        const percent = (x: number) => ((x * 100) / total).toFixed(0)
        const toStr = (x: number) => x === 0 ? '0' : x + ' (' + percent(x) + '%)'
        const provider = this.props.runner.findProvider(player.providerId)
        return {
            player,
            gameType: provider ? provider.name : 'unknown',
            correct: toStr(correct),
            incorrect: toStr(player.incorrectGuesses),
            steals: toStr(steals),
            everybodyDrinks: toStr(player.everybodyDrinksCount),
        }
    }

    private renderPlayerStatsRow(stats: PlayerStatsInfo) {
        return (
            <tr key={stats.player.name}>
                <td>{stats.player.name}</td>
                <td>{stats.correct}</td>
                <td>{stats.incorrect}</td>
                <td>{stats.steals}</td>
                <td>{stats.totalScore}</td>
            </tr>
        )
    }

    private renderCategoryStatsRow(stats: CategoryStatsInfo) {
        return (
            <tr key={stats.player.name}>
                <td>
                    <h5 className='ui header'>
                        <div className='content'>{stats.player.name}</div>
                        <div className='sub header'>{stats.gameType}</div>
                    </h5>
                </td>
                <td>{stats.correct}</td>
                <td>{stats.incorrect}</td>
                <td>{stats.steals}</td>
                <td>{stats.everybodyDrinks}</td>
            </tr>
        )
    }

    private newGame() {
        common.goToHash('new')
    }

    private continueGame() {
        this.props.game.numTurns = 0 // unlimited
        common.goToHash('play')
    }

    private reconfigureGame() {
        common.goToHash('config')
    }
}
