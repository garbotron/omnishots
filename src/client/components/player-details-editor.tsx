import * as React from 'react'
import * as model from '../../common/model.js'
import * as common from '../common.js'

interface FilterState {
    id: string
    enabled: boolean
    values: number[]
}

export interface PlayerDetailsEditorProps {
    runner: common.GameRunner
    initialProviderId: string
    initialFilterValues: model.FilterValue[]
    providerChanged: (providerId: string) => void
    filterValuesChanged: (values: model.FilterValue[]) => void
}

export interface PlayerDetailsEditorState {
    editingFilterIndex: number
    displayFilter: string
    provider: model.ProviderInfo | undefined
    filterState: FilterState[]
}

export class PlayerDetailsEditorComponent extends React.Component<PlayerDetailsEditorProps, PlayerDetailsEditorState> {
    private refCallbacks: (() => void)[] = []

    constructor(props: PlayerDetailsEditorProps) {
        super(props)
        this.state = {
            displayFilter: '',
            editingFilterIndex: -1,
            provider: undefined,
            filterState: [],
        }
    }

    public componentWillMount() {
        const provider = this.props.runner.findProvider(this.props.initialProviderId)
        if (provider) {
            const filterState: FilterState[] = provider.filters.map((x) => ({
                id: x.id,
                enabled: false,
                values: x.defaultValues,
            }))
            for (const fv of this.props.initialFilterValues) {
                filterState.filter((x) => x.id === fv.filterId).forEach((x) => {
                    x.enabled = true
                    x.values = fv.values
                })
            }
            this.setState({ provider, filterState })
        }
    }

    public componentDidMount() {
        this.refCallbacks.map((x) => x())
    }

    public componentDidUpdate() {
        this.componentDidMount()
    }

    public render() {
        const providers = this.props.runner.getProviders()

        if (!this.state.provider) {
            this.props.runner.showError('Invalid provider')
            return <div />
        }

        const providerItems = providers.providers.map((x) =>
            <div key={x.id} data-value={x.id} className='item'>{x.name}</div>)

        const presetItems = this.props.runner.getPresets().presets.map((x, i) =>
            <div key={i} data-value={i} className='item'>{x.name}</div>)

        const getItemClass = (i: number) => {
            if (i === this.state.editingFilterIndex) {
                return 'omnishots-active item'
            } else if (this.state.filterState[i].enabled) {
                return 'omnishots-enabled item'
            } else {
                return 'omnishots-disabled item'
            }
        }

        const getIconClass = (i: number) => this.state.filterState[i].enabled
            ? 'large check circle icon'
            : 'large circle icon'

        const filterSelectorItems = this.state.provider.filters.map((x, i) =>
            <div
                key={i}
                className={getItemClass(i)}
                onClick={() => this.filterListClicked(i)}>
                <i className={getIconClass(i)}></i>
                <div className='content'>
                    <div className='header'>{x.name}</div>
                </div>
            </div>,
        )

        const providerDropdownRef = this.addRef((x: HTMLDivElement) => {
            $(x).dropdown({ onChange: (y) => this.changeProvider(y) })
        })

        const presetDropdownRef = this.addRef((x: HTMLDivElement) => {
            $(x).dropdown({ onChange: (y) => this.applyPreset(y) })
        })

        return (
            <div className='ui form' style={{ minHeight: '15em' }}>
                <div className='two fields'>
                    <div className='field'>
                        <label>Game Type</label>
                        <div className='ui selection dropdown' ref={providerDropdownRef}>
                            <input type='hidden' value={this.state.provider.id} />
                            <i className='dropdown icon'></i>
                            <div className='default text'>Select game type</div>
                            <div className='menu'>{providerItems}</div>
                        </div>
                    </div>
                    <div className='field'>
                        <label>Filter Preset</label>
                        <div className='ui selection dropdown' ref={presetDropdownRef}>
                            <i className='dropdown icon'></i>
                            <div className='default text'>No preset selected</div>
                            <div className='menu'>{presetItems}</div>
                        </div>
                    </div>
                </div>
                <div className='ui grid'>
                    <div className='seven wide column'>
                        <div className='ui middle aligned divided omnishots-filter list'>
                            {filterSelectorItems}
                        </div>
                    </div>
                    {this.renderFilterEditorColumn(this.state.provider, this.state.editingFilterIndex)}
                </div>
            </div>
        )
    }

    private renderFilterEditorColumn(provider: model.ProviderInfo, index: number) {
        if (provider.filters.length === 0) {
            return <h3 className='ui center aligned disabled header'>(no settings available for this game type)</h3>
        }
        if (index < 0 || index >= provider.filters.length) {
            return <h3 className='ui center aligned disabled header'>please select a setting on the left</h3>
        }

        const onRef = this.addRef((x: HTMLDivElement) => {
            $(x).checkbox({ onChange: () => this.setFilterEnabled(index, !val.enabled) })
        })

        const val = this.state.filterState[index]
        return (
            // Note: The onChange handler is here just to make react happy.
            // The real work happens in the semantic UI checkbox change handler.
            <div className='nine wide column'>
                <div className='ui toggle checkbox header' ref={onRef}>
                    <input
                        type='checkbox'
                        tabIndex={0}
                        className='hidden'
                        checked={val.enabled}
                        onChange={() => { return }} />
                    <label style={val.enabled ? {} : { color: '#808080' }}>{provider.filters[index].name}</label>
                </div>
                {this.renderFilterField1(provider, index)}
                {this.renderFilterField2(provider, index)}
            </div>
        )
    }

    private renderFilterField1(provider: model.ProviderInfo, index: number) {
        const filter = provider.filters[index]
        const val = this.state.filterState[index]
        if (!val.enabled) {
            return <div />
        }
        switch (filter.type) {
            case model.FilterType.Number:
                return (
                    <div className='field'>
                        <label>{filter.prompt}</label>
                        <input
                            type='text'
                            defaultValue={val.values[0]}
                            onChange={(e: any) => {
                                const num = +e.target.value
                                if (!isNaN(num)) {
                                    this.setFilterValue(index, 0, num)
                                }
                            }} />
                    </div>
                )
            case model.FilterType.NumberRange:
                return (
                    <div className='field'>
                        <label>{filter.prompt} between</label>
                        <input
                            type='text'
                            defaultValue={val.values[0]}
                            onChange={(e: any) => {
                                const num = Number(e.target.value)
                                if (!isNaN(num)) {
                                    this.setFilterValue(index, 0, num)
                                }
                            }} />
                    </div>
                )
            case model.FilterType.SelectOne:
                const singleItems = filter.names.map((x, i) => <div key={i} data-value={i} className='item'>{x}</div>)
                const singleRef = this.addRef((x: HTMLDivElement) => {
                    $(x).dropdown({
                        onChange: (y) => this.setFilterValue(index, 0, +y),
                    })
                })
                return (
                    <div className='ui fluid selection dropdown' ref={singleRef}>
                        <input type='hidden' value={val.values[0]} />
                        <i className='dropdown icon'></i>
                        <div className='default text'>Select a value</div>
                        <div className='menu'>{singleItems}</div>
                    </div>
                )
            case model.FilterType.SelectMany:
                let indexedFilters = filter.names.map((x, i) => ({ index: i, name: x }))
                let searchField: JSX.Element | undefined
                if (filter.names.length > 25) {
                    // if there are enough entries, add a search filter textbox
                    indexedFilters = indexedFilters.filter((x) => {
                        for (const word of this.state.displayFilter.split(' ')) {
                            if (x.name.toLowerCase().indexOf(word.trim().toLowerCase()) < 0) {
                                return false
                            }
                        }
                        return true
                    })
                    searchField = (
                        <div className='field'>
                            <div className='ui fluid icon input'>
                                <input
                                    type='text'
                                    placeholder='Filter...'
                                    value={this.state.displayFilter}
                                    onChange={(e: any) => this.setState({
                                        displayFilter: e.target.value,
                                        editingFilterIndex: this.state.editingFilterIndex,
                                    })} />
                                <i className='search icon'></i>
                            </div>
                        </div>
                    )
                }

                const multiItems = indexedFilters.map((x) => <option key={x.index} value={x.index}>{x.name}</option>)
                return (
                    <div>
                        {searchField}
                        <select
                            multiple={true}
                            style={{ minHeight: '15em' }}
                            value={val.values.map((x) => x.toString())}
                            onChange={(e: any) =>
                                this.setFilterValues(index, ($(e.target).val() as string[]).map((x) => +x))}>
                            {multiItems}
                        </select>
                    </div>
                )
            default:
                return undefined
        }
    }

    private renderFilterField2(provider: model.ProviderInfo, index: number) {
        const filter = provider.filters[index]
        const val = this.state.filterState[index]
        if (!val.enabled) {
            return <div />
        }
        switch (filter.type) {
            case model.FilterType.NumberRange:
                return (
                    <div className='field'>
                        <label>and</label>
                        <input
                            type='text'
                            defaultValue={val.values[1]}
                            onChange={(e: any) => {
                                const num = Number(e.target.value)
                                if (!isNaN(num)) {
                                    this.setFilterValue(index, 1, num)
                                }
                            }} />
                    </div>
                )
            default:
                return <div />
        }
    }

    private addRef(cb: (x: HTMLDivElement) => void): (x: HTMLDivElement) => void {
        return (x: HTMLDivElement) => this.refCallbacks.push(() => cb(x))
    }

    private changeProvider(id: string) {
        const provider = this.props.runner.findProvider(id)
        if (provider) {
            const filterState = provider.filters.map((x) => ({ id: x.id, enabled: false, values: x.defaultValues }))
            this.setState({ provider, filterState })
            this.props.providerChanged(id)
            this.props.filterValuesChanged([])
        } else {
            this.props.runner.showError('Invalid provider')
        }
    }

    private applyPreset(presetIndex: number) {
        const presets = this.props.runner.getPresets().presets
        if (presetIndex >= presets.length) {
            this.props.runner.showError('Invalid preset')
        } else {
            const preset = presets[presetIndex]
            const provider = this.props.runner.findProvider(preset.providerId)
            if (!provider) {
                this.props.runner.showError('Invalid preset provider')
            } else {
                const filterState: FilterState[] = []
                for (const filter of provider.filters) {
                    const presetValue = preset.filterValues.find((x) => x.filterId === filter.id)
                    filterState.push({
                        id: filter.id,
                        enabled: presetValue !== undefined,
                        values: presetValue ? presetValue.values : filter.defaultValues,
                    })
                }
                this.setState({ provider, filterState })
                this.props.providerChanged(preset.providerId)
                this.props.filterValuesChanged(preset.filterValues)
            }
        }
    }

    private setFilterEnabled(index: number, enabled: boolean) {
        const filterState = this.state.filterState.slice()
        filterState[index].enabled = enabled
        this.setState({
            editingFilterIndex: index,
            filterState,
        })
        this.notifyFilterValuesChanged()
    }

    private setFilterValue(index: number, subIndex: number, value: number) {
        const filterState = this.state.filterState.slice()
        filterState[index].values[subIndex] = value
        this.setState({
            editingFilterIndex: index,
            filterState,
        })
        this.notifyFilterValuesChanged()
    }

    private setFilterValues(index: number, values: number[]) {
        const filterState = this.state.filterState.slice()
        filterState[index].values = values
        this.setState({
            editingFilterIndex: index,
            filterState,
        })
        this.notifyFilterValuesChanged()
    }

    private notifyFilterValuesChanged() {
        this.props.filterValuesChanged(
            this.state.filterState.filter((x) => x.enabled).map((x) => ({ filterId: x.id, values: x.values })))
    }

    private filterListClicked(index: number) {
        // - if you click on a filter that isn't already selected, enable it and select it
        // - if you click on the filter that's already selected, toggle it
        if (this.state.editingFilterIndex === index) {
            this.setFilterEnabled(index, !this.state.filterState[index].enabled)
        } else {
            this.setFilterEnabled(index, true)
        }
    }
}
