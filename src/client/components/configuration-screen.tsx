import * as React from 'react'
import { ReactSortable } from 'react-sortablejs'
import * as model from '../../common/model.js'
import * as common from '../common.js'
import { PlayerDetailsEditorComponent } from './player-details-editor.js'

export interface ConfigurationScreenProps {
    runner: common.GameRunner
    game: model.Game
}

export interface ConfigurationScreenState {
    game: model.Game | undefined
    resultCounts: (model.FilterTestResult | undefined)[]
    isEditing: boolean
    editPlayerIndex: number
    editPlayerProviderId: string
    editPlayerFilterValues: model.FilterValue[]
    newPlayerName: string
    newPlayerProviderId: string
    newPlayerFilterValues: model.FilterValue[]
    rearrangePlayers: { id: number, player: model.Player }[]
}

export class ConfigurationScreenComponent extends React.Component<ConfigurationScreenProps, ConfigurationScreenState> {
    private menuDropdown: HTMLDivElement | undefined = undefined
    private gameLengthDropdown: HTMLDivElement | undefined = undefined
    private gameLengthSpan: HTMLSpanElement | undefined = undefined
    private newPlayerModal: HTMLDivElement | undefined = undefined
    private newPlayerForm: HTMLDivElement | undefined = undefined
    private newPlayerProviderDropdown: HTMLDivElement | undefined = undefined
    private newPlayerPresetDropdown: HTMLDivElement | undefined = undefined
    private rearrangeModal: HTMLDivElement | undefined = undefined

    constructor(props: ConfigurationScreenProps) {
        super(props)
        this.state = {
            game: undefined,
            resultCounts: [],
            isEditing: false,
            editPlayerIndex: -1,
            editPlayerProviderId: '',
            editPlayerFilterValues: [],
            newPlayerName: '',
            newPlayerProviderId: '',
            newPlayerFilterValues: [],
            rearrangePlayers: [],
        }
    }

    public componentWillMount() {
        this.setState({ game: common.deepCopy(this.props.game) })
    }

    public componentDidMount() {
        this.setupElements()
        for (let i = 0; i < this.state.game!.players.length; i++) {
            this.updateResultCount(i)
        }
    }

    public componentDidUpdate() {
        this.setupElements()
    }

    public render() {
        let mainDiv: JSX.Element
        if (this.state.isEditing) {
            const player = this.state.game!.players[this.state.editPlayerIndex]
            mainDiv = (
                <div className='ui left aligned segment'>
                    <PlayerDetailsEditorComponent
                        runner={this.props.runner}
                        initialProviderId={player.providerId}
                        initialFilterValues={player.filterValues}
                        providerChanged={(x) => this.editProviderIdChanged(x)}
                        filterValuesChanged={(x) => this.editFilterValuesChanged(x)} />
                    <div className='ui basic right aligned segment' style={{ padding: 0 }}>
                        <button
                            className='ui primary button'
                            style={{ marginTop: '1em' }}
                            onClick={() => this.playerEditApply()}>Apply Changes</button>
                        <button className='ui button' onClick={() => this.playerEditDiscard()}>Discard Changes</button>
                    </div>
                </div>
            )
        } else {
            const turnCounts = [0, 10, 25, 50, 100, 200, 500]
            const turnCountItems = turnCounts.map(
                (x) => <div key={x} data-value={x} className='item'>{this.getNumTurnsPickerTitle(x)}</div>)

            const providers = this.props.runner.getProviders()
            const providerItems = providers.providers.map((x) =>
                <div key={x.id} data-value={x.id} className='item'>{x.name}</div>)

            const presets = this.props.runner.getPresets()
            const presetItems = presets.presets.map((x, i) =>
                <div key={i} data-value={i} className='item'>{x.name}</div>)

            const newPlayerModal = (
                <div className='ui small modal' ref={(x) => this.newPlayerModal = x || undefined}>
                    <div className='header'>Add New Player</div>
                    <div className='content'>
                        <div className='ui form' ref={(x) => this.newPlayerForm = x || undefined}>
                            <div className='field'>
                                <label>Player Name</label>
                                <input
                                    type='text'
                                    name='name'
                                    placeholder='Player Name'
                                    onChange={(e: any) => this.setNewPlayerName(e.target.value)}>
                                </input>
                            </div>
                            <div className='two fields'>
                                <div className='field'>
                                    <label>Choose Game Type</label>
                                    <div className='ui selection dropdown'
                                        ref={(x) => this.newPlayerProviderDropdown = x || undefined}>
                                        <input name='provider' type='hidden' value={this.state.newPlayerProviderId} />
                                        <i className='dropdown icon'></i>
                                        <div className='default text'>Select game type</div>
                                        <div className='menu'>{providerItems}</div>
                                    </div>
                                </div>
                                <div className='field'>
                                    <label>Or Load a Preset</label>
                                    <div className='ui selection dropdown'
                                        ref={(x) => this.newPlayerPresetDropdown = x || undefined}>
                                        <i className='dropdown icon'></i>
                                        <div className='default text'>No preset selected</div>
                                        <div className='menu'>{presetItems}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='actions'>
                        <button className='ui positive button'>Configure {this.state.newPlayerName}</button>
                        <button className='ui deny button'>Cancel</button>
                    </div>
                </div>
            )

            const rearrangeModal = (
                <div className='ui small modal' ref={(x) => this.rearrangeModal = x || undefined}>
                    <div className='header'>Rearrange Players</div>
                    <div className='content'>
                        <p>Drag and drop to rearrange the player order.</p>
                        <ReactSortable
                            className='ui list'
                            list={this.state.rearrangePlayers}
                            setList={(x) => this.setState({ rearrangePlayers: x })}>
                            {this.state.rearrangePlayers.map((x) =>
                                <div className='rearrangeable item' key={x.id}>
                                    <div className='header'>{x.player.name}</div>
                                    <div className='description'>{this.getProviderSummary(x.player)}</div>
                                </div>)}
                        </ReactSortable>
                    </div>
                    <div className='actions'>
                        <button className='ui positive button'>Apply</button>
                        <button className='ui deny button'>Cancel</button>
                    </div>
                </div>
            )

            const playerRows = this.state.game!.players.map((x, i) =>
                <tr key={i}>
                    <td className='ui small header'>{x.name}</td>
                    <td>{this.getProviderSummary(x)}</td>
                    {this.renderResultCountCell(i)}
                    <td className='right aligned collapsing selectable'>
                        <a href='#' title='Edit Configuration'
                            onClick={(e) => {
                                e.preventDefault()
                                this.editPlayerIndex(i)
                            }}>
                            <i className='large edit icon'></i>
                        </a>
                    </td>
                    <td className='right aligned collapsing negative selectable'>
                        <a href='#' title='Delete Player'
                            onClick={(e) => {
                                e.preventDefault()
                                this.deletePlayerIndex(i)
                            }}>
                            <i className='large delete icon'></i>
                        </a>
                    </td>
                </tr>,
            )

            mainDiv = (
                <div className='ui left aligned clearing segment'>
                    <div className='ui menu'>
                        <div className='ui dropdown item' ref={(x) => this.gameLengthDropdown = x || undefined}>
                            <i className='clock icon'></i>
                            <span className='text' ref={(x) => this.gameLengthSpan = x  || undefined}>
                                {this.getNumTurnsPickerTitle(this.state.game!.numTurns)}
                            </span>
                            <div className='menu'>{turnCountItems}</div>
                        </div>
                        <div className='right menu'>
                            {newPlayerModal}
                            {rearrangeModal}
                            <a className='item' onClick={() => this.openNewPlayerModal()}>
                                <i className='green user icon'></i>Add Player
                            </a>
                            <div
                                className='ui dropdown link icon item'
                                ref={(x) => this.menuDropdown = x || undefined}>
                                <i className='ellipsis vertical icon'></i>
                                <div className='menu'>
                                    <a className='item' onClick={() => common.goToHash('presets')}>
                                        <i className='filter icon'></i>Edit Filter Presets
                                    </a>
                                {this.state.game!.players.length > 1
                                    ? <a className='item' onClick={() => this.openRearrangeModal()}>
                                        <i className='sort content descending icon'></i>Rearrange Players
                                    </a>
                                    : undefined}
                                </div>
                            </div>
                        </div>
                    </div>
                    <table className='ui very compact single line celled table'>
                        <thead>
                            <tr>
                                <th>Player</th>
                                <th>Game Type</th>
                                <th>Filter Result</th>
                                <th className='collapsing'></th>
                                <th className='collapsing'></th>
                            </tr>
                        </thead>
                        <tbody>{playerRows}</tbody>
                    </table>

                    <button
                        className='ui right floated button'
                        onClick={() => this.discardChanges()}>
                        Discard Changes
                    </button>
                    <button
                        className='ui right floated primary button'
                        onClick={() => this.commitChanges()}>
                        Save Changes
                    </button>
                </div>
            )
        }

        return <div className='omnishots-main column'>
            {this.props.runner.renderOverlays()}
            {this.props.runner.renderToolbar(common.Screen.Configuration)}
            {mainDiv}
        </div>
    }

    private setupElements() {
        $(this.menuDropdown!).dropdown()
        $(this.newPlayerForm!).form({ fields: { name: 'empty', provider: 'empty' } })
        $(this.newPlayerProviderDropdown!).dropdown({
            onChange: (x: string) => this.setState({ newPlayerProviderId: x, newPlayerFilterValues: [] }),
        })
        $(this.newPlayerPresetDropdown!).dropdown({
            onChange: (x: number) => {
                const presets = this.props.runner.getPresets()
                if (x < presets.presets.length) {
                    const preset = presets.presets[x]
                    this.setState({
                        newPlayerProviderId: preset.providerId,
                        newPlayerFilterValues: preset.filterValues,
                    })
                }
            }
        })
        $(this.gameLengthDropdown!).dropdown({
            onChange: (x: string) => {
                this.state.game!.numTurns = +x
                $(this.gameLengthSpan!).text(this.getNumTurnsPickerTitle(this.state.game!.numTurns))
            },
        })
    }

    private renderResultCountCell(playerIndex: number) {
        if (playerIndex >= this.state.resultCounts.length) {
            return <td><div className='ui active tiny inline loader'></div></td>
        }
        const count = this.state.resultCounts[playerIndex]
        const fmt = Intl.NumberFormat(undefined, { maximumFractionDigits: 1 })
        if (!count) {
            return <td><div className='ui active tiny inline loader'></div></td>
        } else if (count.filteredCount === 'UNKNOWN') {
            return <td className='disabled'>unknown</td>
        } else if (count.totalCount === 'UNKNOWN') {
            return <td>{count.approximate ? '~' : ''}{fmt.format(count.filteredCount)}</td>
        } else {
            const num = fmt.format(count.filteredCount)
            const percent = fmt.format((count.filteredCount * 100) / count.totalCount)
            return <td>{count.approximate ? '~' : ''}{num} ({percent}%)</td>
        }
    }

    private getNumTurnsPickerTitle(turns: number) {
        return 'Game length: ' + (turns === 0 ? 'unlimited' : `${turns} turns`)
    }

    private playerEditApply() {
        const player = this.state.game!.players[this.state.editPlayerIndex]
        player.providerId = this.state.editPlayerProviderId
        player.filterValues = this.state.editPlayerFilterValues
        const resultCounts = common.deepCopy(this.state.resultCounts)
        resultCounts[this.state.editPlayerIndex] = undefined
        this.setState({ resultCounts, isEditing: false })
        this.updateResultCount(this.state.editPlayerIndex)
    }

    private playerEditDiscard() {
        this.setState({ isEditing: false })
    }

    private openNewPlayerModal() {
        $(this.newPlayerModal!).modal({
            onApprove: () => {
                if (!$(this.newPlayerForm!).form('is valid')) {
                    return false
                }
                this.addNewPlayer(
                    this.state.newPlayerName,
                    this.state.newPlayerProviderId,
                    this.state.newPlayerFilterValues)
                return
            },
        }).modal('show')
    }

    private openRearrangeModal() {
        this.setState({ rearrangePlayers: this.state.game!.players.map((x, i) => ({ id: i, player: x }))})
        $(this.rearrangeModal!).modal({
            onApprove: () => {
                const game = common.deepCopy(this.state.game!)
                const resultCounts = common.deepCopy(this.state.resultCounts)
                for (let i = 0; i < game.players.length; ++i) {
                    const newIdx = this.state.rearrangePlayers.findIndex(x => x.id === i)
                    game.players[newIdx] = common.deepCopy(this.state.game!.players[i])
                    resultCounts[newIdx] = common.deepCopy(this.state.resultCounts[i])
                }
                this.setState({ game, resultCounts })
            },
        }).modal('show')
    }

    private setNewPlayerName(name: string) {
        this.setState({ newPlayerName: name })
    }

    private addNewPlayer(name: string, providerId: string, filterValues: model.FilterValue[]) {
        const provider = this.props.runner.findProvider(providerId)
        if (!provider) {
            this.props.runner.showError('bad provider')
            return
        }
        const game = common.deepCopy(this.state.game!)
        const player: model.Player = {
            name,
            providerId,
            filterValues,
            perPlayerScores: [],
            incorrectGuesses: 0,
            everybodyDrinksCount: 0,
        }
        game.players.push(player)

        // open editor unless there are no filters or we loaded a preset
        if (provider.filters.length > 0 && filterValues.length === 0) {
            this.setState({
                game,
                isEditing: true,
                editPlayerIndex: game.players.length - 1,
                editPlayerProviderId: providerId,
                editPlayerFilterValues: filterValues,
                newPlayerName: '',
                newPlayerProviderId: '',
            })
        } else {
            this.setState({
                game,
                newPlayerName: '',
                newPlayerProviderId: '',
            })
            this.updateResultCount(game.players.length - 1)
        }
    }

    private editPlayerIndex(index: number) {
        this.setState({
            isEditing: true,
            editPlayerIndex: index,
            editPlayerProviderId: this.state.game!.players[index].providerId,
            editPlayerFilterValues: this.state.game!.players[index].filterValues.map(common.deepCopy),
        })
    }

    private deletePlayerIndex(index: number) {
        const game = common.deepCopy(this.state.game!)
        game.players.splice(index, 1)
        this.setState({ game })
    }

    private editProviderIdChanged(providerId: string) {
        this.setState({ editPlayerProviderId: providerId })
    }

    private editFilterValuesChanged(values: model.FilterValue[]) {
        this.setState({ editPlayerFilterValues: values })
    }

    private discardChanges() {
        common.goToHash('play')
    }

    private commitChanges() {
        // reset stats since the categories changed
        this.state.game!.currentTurn = 0
        for (const player of this.state.game!.players) {
            player.perPlayerScores = []
            player.incorrectGuesses = 0
            player.everybodyDrinksCount = 0
        }

        // move on to the next player since the game is being re-configured
        this.state.game!.currentPlayerIndex++
        if (this.state.game!.currentPlayerIndex >= this.state.game!.players.length) {
            this.state.game!.currentPlayerIndex = 0
        }

        // now we can finally update the game object for reals
        Object.assign(this.props.game, this.state.game!)
        common.saveGame(this.props.game)
        common.goToHash('play')
    }

    private getProviderSummary(player: model.Player) {
        const provider = this.props.runner.findProvider(player.providerId)
        if (!provider) {
            return 'Unknown'
        } else if (player.filterValues.length === 0) {
            return provider.name
        } else if (player.filterValues.length === 1) {
            return `${provider.name} - 1 setting`
        } else {
            return `${provider.name} - ${player.filterValues.length} settings`
        }
    }

    private updateResultCount(playerIndex: number) {
        const player = this.state.game!.players[playerIndex]
        const input: model.ContentQuery = {
            providerId: player.providerId,
            filterValues: player.filterValues,
        }
        common.post<model.FilterTestResult>('test', input)
            .then((result) => {
                const resultCounts = common.deepCopy(this.state.resultCounts)
                while (playerIndex >= resultCounts.length) {
                    resultCounts.push({ filteredCount: 'UNKNOWN', totalCount: 'UNKNOWN' })
                }
                resultCounts[playerIndex] = result
                this.setState({ resultCounts })
            })
            .catch((err) => this.props.runner.showError(err))
    }
}
