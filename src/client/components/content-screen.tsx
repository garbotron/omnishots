import * as React from 'react'
import * as model from '../../common/model.js'
import * as common from '../common.js'

const photobombChance = 0.02

export interface ContentScreenProps {
    runner: common.GameRunner
    game: model.Game
}

export interface ContentScreenState {
    content: model.Content | undefined
    showingSolution: boolean
    showingStealMenu: boolean
    hudHidden: boolean
    isPhotobombed: boolean
}

export class ContentScreenComponent extends React.Component<ContentScreenProps, ContentScreenState> {
    constructor(props: ContentScreenProps) {
        super(props)
        this.state = {
            content: undefined,
            showingSolution: false,
            showingStealMenu: false,
            hudHidden: false,
            isPhotobombed: false,
        }
    }

    public componentDidMount() {
        document.addEventListener('keyup', this.documentKeyUp)
        this.showNewContent()
    }

    public componentWillUnmount() {
        document.removeEventListener('keyup', this.documentKeyUp)
    }

    public render() {
        if (!this.state.content) {
            return <div>{this.props.runner.renderOverlays()}</div>
        }

        if (this.props.game.currentPlayerIndex >= this.props.game.players.length) {
            return <div>ERROR: BAD CURRENT PLAYER INDEX</div>
        }
        const player = this.props.game.players[this.props.game.currentPlayerIndex]
        const provider = this.props.runner.findProvider(player.providerId)
        if (!provider) {
            return <div>ERROR: BAD PROVIDER NAME</div>
        }

        let content: string
        let solution: string
        let detailsUrl: string
        if (this.state.content.result === 'NOT_FOUND') {
            content = '<div class="ui huge negative omnishots-content-not-found message">'
                + 'Nothing was found for this category.<br>'
                + 'Go to the <em>Configure</em> menu to reconfigure the settings.</div>'
            solution = 'Fix your configuration!'
            detailsUrl = ''
        } else {
            content = this.state.content.content
            solution = this.state.content.solution
            detailsUrl = this.state.content.detailsUrl
        }

        const innerHtml = { __html: content }

        let stealMenu: JSX.Element
        if (this.state.showingStealMenu) {
            const items = this.props.game.players
                .filter((_, i) => i !== this.props.game.currentPlayerIndex)
                .map((x, i) => {
                    const letter = String.fromCharCode(97 + i)
                    return <button
                        key={i}
                        className={`ui compact button keycode-${65 + i}`}
                        onClick={() => this.submitCorrect(x)}>
                        {`${letter}) ${x.name}`}
                    </button>
            })
            stealMenu = <div>{items}</div>
        } else {
            stealMenu = <div />
        }

        const visibilityClass = this.state.hudHidden ? 'hidden' : 'visible'
        return <div className='column' style={{ height: '100%', padding: 0 }}>
            <div className='omnishots-content-holder' dangerouslySetInnerHTML={innerHtml} />
            <div
                className='omnishots-photobomb'
                style={{ visibility: this.state.isPhotobombed ? 'visible' : 'collapse' }} />
            {this.props.runner.renderOverlays()}
            {this.props.runner.renderToolbar(common.Screen.Content, visibilityClass)}
            <div
                className='omnishots-content-hide keycode-72'
                title={this.state.hudHidden ? 'Show HUD' : 'Hide HUD'}
                onClick={() => this.toggleHudVisibility()}>
                <i className='hide icon' />
            </div>
            {this.state.showingSolution
                ? <div className={`omnishots-content-solution ${visibilityClass}`}>
                    <div className='ui compact piled segment'>
                        <span dangerouslySetInnerHTML={{ __html: solution }} />
                        <sup>
                            <a href={detailsUrl} title={provider.elemDetailsTitle}>
                                <i className='info link icon'></i>
                            </a>
                        </sup>
                        <div>
                            <button
                                className='ui compact positive button keycode-49'
                                onClick={() => this.submitCorrect(player)}>
                                1) got it
                            </button>
                            <button
                                className='ui compact negative button keycode-50'
                                onClick={() => this.submitIncorrect()}>
                                2) didn't get it
                            </button>
                            <button
                                className='ui compact button keycode-51'
                                onClick={() => this.showNewContent()}>
                                3) retry
                            </button>
                            <button
                                className='ui compact button keycode-52'
                                onClick={() => this.showStealMenu()}>
                                4) steal
                            </button>
                            <button
                                className='ui compact button keycode-53'
                                onClick={() => this.submitEverybodyDrinks()}>
                                5) everybody drinks
                            </button>
                        </div>
                        {stealMenu}
                    </div>
                </div>
                : <div
                    className={`omnishots-content-prompt keycode-32 ${visibilityClass}`}
                    onClick={() => this.showSolution()}>
                    {player.name}! {provider.prompt}
                </div>
            }
        </div>
    }

    private documentKeyUp(e: KeyboardEvent) {
        // only handle the standard keybindings if no modals are active
        let modalActive = false
        $('.modal').each((_, x) => {
            if ($(x).modal('is active')) {
                modalActive = true
            }
        })
        if (!modalActive) {
            $(`.keycode-${e.keyCode}`).click()
        }
    }

    private toggleHudVisibility() {
        this.setState({ hudHidden: !this.state.hudHidden })
    }

    private showNewContent() {
        if (this.props.game.currentPlayerIndex >= this.props.game.players.length) {
            return
        }
        const player = this.props.game.players[this.props.game.currentPlayerIndex]
        const input: model.ContentQuery = {
            providerId: player.providerId,
            filterValues: player.filterValues,
        }
        this.props.runner.showLoading('loading content')
        common.post<model.Content>('content', input)
            .then((content) => {
                this.setState({
                    content,
                    showingSolution: false,
                    showingStealMenu: false,
                    hudHidden: false,
                    isPhotobombed: Math.random() < photobombChance,
                })
                this.props.runner.hideLoading()
            })
            .catch((err) => this.props.runner.showError(err))
    }

    private submitCorrect(player: model.Player) {
        // the player got it right, increment the relevant statistic and set the current player index
        const playerIndex = this.props.game.players.indexOf(player)
        if (playerIndex >= 0) {
            this.props.game.players[this.props.game.currentPlayerIndex].perPlayerScores[playerIndex] =
                (this.props.game.players[this.props.game.currentPlayerIndex].perPlayerScores[playerIndex] || 0) + 1
            this.props.game.currentPlayerIndex = playerIndex
            this.advanceTurn()
        }
    }

    private submitIncorrect() {
        // the player got it wrong, increment the stat and move on to the next player
        this.props.game.players[this.props.game.currentPlayerIndex].incorrectGuesses++
        this.props.game.currentPlayerIndex++
        if (this.props.game.currentPlayerIndex >= this.props.game.players.length) {
            this.props.game.currentPlayerIndex = 0
        }
        this.advanceTurn()
    }

    private submitEverybodyDrinks() {
        // it's an everybody drinks, so just increment the stat and reload with new content
        this.props.game.players[this.props.game.currentPlayerIndex].everybodyDrinksCount++
        this.advanceTurn()
    }

    private advanceTurn() {
        this.props.game.currentTurn++
        common.saveGame(this.props.game)
        if (this.props.game.numTurns !== 0 && this.props.game.currentTurn >= this.props.game.numTurns) {
            common.goToHash('stats')
        } else {
            this.showNewContent()
        }
    }

    private showSolution() {
        this.setState({ showingSolution: true, hudHidden: false })
    }

    private showStealMenu() {
        this.setState({ showingStealMenu: true, hudHidden: false })
    }
}
