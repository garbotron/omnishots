import * as React from 'react'
import * as common from '../common.js'

export interface SplashScreenProps { runner: common.GameRunner }

export class SplashScreenComponent extends React.Component<SplashScreenProps, unknown> {
    public render() {
        return <div className='omnishots-main column' style={{ minHeight: '32em', textAlign: 'center' }}>
            {this.props.runner.renderOverlays()}
            <div className='ui huge inverted header omnishots-logo'>Omnishots</div>
            <a href='#/new' className='ui huge inverted download button'>
                New Game
            </a>
            {this.renderContinueButton()}
        </div>
    }

    private renderContinueButton() {
        if (common.getSavedGame()) {
            return <a href='#/play' className='ui huge inverted download button'>
                Continue Game
            </a>
        } else {
            return undefined
        }
    }
}
