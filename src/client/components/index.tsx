import * as React from 'react'
import * as model from '../../common/model.js'
import * as common from '../common.js'
import { AboutScreenComponent } from './about-screen.js'
import { ConfigurationScreenComponent } from './configuration-screen.js'
import { PresetsScreenComponent } from './presets-screen.js'
import { ContentScreenComponent } from './content-screen.js'
import { SplashScreenComponent } from './splash-screen.js'
import { StatsScreenComponent } from './stats-screen.js'

interface IndexState {
    screen: common.Screen
    game: model.Game
    error: string | undefined
    loadingText: string | undefined
}

function getDefaultGame(): model.Game {
    return { players: [], currentPlayerIndex: 0, numTurns: 0, currentTurn: 0 }
}

export class IndexComponent extends React.Component<unknown, IndexState> implements common.GameRunner {
    private providers: model.ProviderReport
    private presets: model.FilterPresets
    private errorModal: HTMLElement | undefined = undefined
    private hashChangeListener: () => void

    constructor(props: unknown) {
        super(props)
        this.state = {
            screen: common.Screen.Empty,
            game: common.getSavedGame() || getDefaultGame(),
            error: undefined,
            loadingText: undefined,
        }
    }

    public componentDidMount() {
        this.hashChangeListener = () => this.followHashLocation()
        window.addEventListener('hashchange', this.hashChangeListener, false)

        // Wait until we're done loading providers/presets (other screens will use this info) before loading anything
        this.showLoading('loading providers/presets')

        Promise.all([
            common.get<model.ProviderReport>('providers'),
            common.get<model.FilterPresets>('presets'),
        ]).then(([providers, presets]) => {
            this.providers = providers
            this.presets = presets
            this.followHashLocation()
        }).catch((err) => this.showError(err))
    }

    public componentWillUnmount() {
        window.removeEventListener('hashchange', this.hashChangeListener, false)
    }

    public render() {
        return <div className='ui middle aligned center aligned grid'>{this.renderScreen()}</div>
    }

    public showLoading(text: string) {
        this.setState({ loadingText: text })
    }

    public hideLoading() {
        this.setState({ loadingText: undefined })
    }

    public showError(error: any) {
        let text: string
        if (!error) {
            text = 'unknown error'
        } else if (typeof error === 'string') {
            text = error
        } else {
            text = JSON.stringify(error)
        }
        this.setState({ error: this.formatError(text), loadingText: undefined }, () => {
            if (!this.errorModal) {
                throw new Error('Error modal reference not set correctly')
            }
            $(this.errorModal).modal('show')
        })
    }

    public renderToolbar(activeScreen: common.Screen, extraClass?: string): JSX.Element {
        const renderItem = (screen: common.Screen, title: string, handler: () => void) => {
            if (screen === activeScreen) {
                return <span className='active item'>{title}</span>
            } else {
                return <a className='item' onClick={handler}>{title}</a>
            }
        }

        const header = (this.state.game.currentTurn < this.state.game.numTurns)
            ? `turn ${this.state.game.currentTurn + 1} of ${this.state.game.numTurns}`
            : `turn ${this.state.game.currentTurn + 1}`
        return (
            <div className={`omnishots-nav-holder ${extraClass}`}>
                <div className='ui inverted compact omnishots-nav segment'>
                    <div className='ui inverted secondary menu'>
                        <span className='header item'>{header}</span>
                        {renderItem(common.Screen.Content, 'Play', () => common.goToHash('play'))}
                        {renderItem(common.Screen.Configuration, 'Configure', () => common.goToHash('config'))}
                        {renderItem(common.Screen.Stats, 'Stats', () => common.goToHash('stats'))}
                        {renderItem(common.Screen.Splash, 'Exit', () => common.goToHash(''))}
                        {renderItem(common.Screen.About, 'About', () => common.goToHash('about'))}
                    </div>
                </div>
            </div>
        )
    }

    public renderOverlays(): JSX.Element[] {
        return [
            <div className={`ui ${this.state.loadingText ? 'active' : 'inactive'} page dimmer`}>
                <div className='ui active massive text loader'>{this.state.loadingText}</div>
            </div>,
            <div className='ui small modal' ref={(x) => this.errorModal = x || undefined}>
                <div className='header'>An error has occurred!</div>
                <div className='content'>
                    <h3 className='ui header'>
                        <i className='red warning circle icon'></i>
                        <div className='content'>
                            {this.state.error}
                        </div>
                    </h3>
                </div>
                <div className='actions'>
                    <div className='ui primary ok button'>Close</div>
                </div>
            </div>,
        ]
    }

    public findProvider(id: string): model.ProviderInfo | undefined {
        for (const p of this.getProviders().providers) {
            if (p.id === id) {
                return p
            }
        }
        return undefined
    }

    public getProviders(): model.ProviderReport {
        return this.providers
    }

    public getPresets(): model.FilterPresets {
        return this.presets
    }

    public setPresets(presets: model.FilterPresets): Promise<void> {
        return common.post('presets', presets)
            .then(() => { this.presets = presets })
            .catch((err) => this.showError(err))
    }

    private renderScreen() {
        switch (this.state.screen) {
            case common.Screen.Empty:
                return <div>{this.renderOverlays()}</div>
            case common.Screen.Splash:
                return <SplashScreenComponent runner={this} />
            case common.Screen.Configuration:
                return <ConfigurationScreenComponent runner={this} game={this.state.game} />
            case common.Screen.Presets:
                return <PresetsScreenComponent runner={this} game={this.state.game} />
            case common.Screen.Content:
                return <ContentScreenComponent runner={this} game={this.state.game} />
            case common.Screen.Stats:
                return <StatsScreenComponent runner={this} game={this.state.game} />
            case common.Screen.About:
                return <AboutScreenComponent runner={this} />
            default:
                return <p>Unknown screen: {this.state.screen}</p>
        }
    }

    private followHashLocation() {
        // Removes the '#', and any leading/final '/' characters
        this.hideLoading()
        const loc = window.location.hash.replace(/^#\/?|\/$/g, '').split('/').map(decodeURIComponent)
        switch (loc[0].toLowerCase()) {
            default:
            case '':
                this.setState({ screen: common.Screen.Splash })
                break
            case 'new':
                const game = getDefaultGame()
                common.saveGame(game)
                this.setState({ screen: common.Screen.Configuration, game })
                break
            case 'config':
                this.setState({ screen: common.Screen.Configuration })
                break
            case 'presets':
                this.setState({ screen: common.Screen.Presets })
                break
            case 'play':
                if (this.state.game.players.length) {
                    this.setState({ screen: common.Screen.Content })
                } else {
                    this.setState({ screen: common.Screen.Splash })
                }
                break
            case 'stats':
                this.setState({ screen: common.Screen.Stats })
                break
            case 'about':
                this.setState({ screen: common.Screen.About })
                break
        }
    }

    private formatError(s: string) {
        s = s.trim()
        if (s.length > 0) {
            s = s.charAt(0).toUpperCase() + s.slice(1)
        }
        if (!s.endsWith('.')) {
            s += '.'
        }
        return s
    }
}
