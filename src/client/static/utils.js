// eslint-disable-next-line
function setupAudioClip(audio, lenSecs) {
    var firstPossibleSec = 0;
    var lastPossibleSec = audio.duration - lenSecs;
    var startSec = firstPossibleSec + (Math.random() * (lastPossibleSec - firstPossibleSec));
    audio.currentTime = startSec;
    audio.play();
    audio.ontimeupdate = function() {
        if (audio.currentTime > startSec + lenSecs) {
            audio.pause();
        }
    };
    audio.onplay = function() {
        if (audio.currentTime < startSec || audio.currentTime > startSec + lenSecs) {
            audio.currentTime = startSec;
        }
    };
}
