import * as React from 'react'
import * as ReactDOM from 'react-dom'

import { IndexComponent } from './components/index.js'

ReactDOM.render(<IndexComponent />, document.getElementById('root'))
