#!/usr/bin/env node
import { App, Stack, StackProps, Duration, aws_apigateway as apigateway, aws_lambda as lambda } from 'aws-cdk-lib'
import { Construct } from 'constructs'
import * as funcs from '../server/functions.js'

function requireEnvironmentVariables(names: string[]) {
    const vars: { [key: string]: string } = {}
    for (const name of names) {
        const v = process.env[name]
        if (!v) {
            console.error(`${name} not set`)
            process.exit(1)
        }
        vars[name] = v
    }
    return vars
}

class OmnishotsStack extends Stack {
    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props)

        const api = new apigateway.RestApi(this, 'omnishots-api', {
            restApiName: 'Omnishots API',
            defaultCorsPreflightOptions: { allowOrigins: apigateway.Cors.ALL_ORIGINS }
        })
        const code = lambda.Code.fromAsset('.build/server')

        for (const func of funcs.getAllServerFunctions()) {
            const resource = api.root.addResource(func.name)
            for (const method of func.methods) {
                const handler = new lambda.Function(
                    this,
                    `${func.name}-${method.method.toLowerCase()}`, {
                        timeout: Duration.seconds(15),
                        functionName: `omnishots-cdk-${func.name}-${method.method.toLowerCase()}`,
                        runtime: lambda.Runtime.NODEJS_20_X,
                        code,
                        handler: `functions.${method.handlerName}`,
                        environment: requireEnvironmentVariables([
                            'OMNISHOTS_CORE_MONGODB_URI',
                            'OMNISHOTS_GAMES_MONGODB_URI',
                            'OMNISHOTS_ANIME_MONGODB_URI',
                            'OMNISHOTS_JEOPARDY_MONGODB_URI',
                            'OMNISHOTS_MOVIES_API_KEY',
                            'OMNISHOTS_DICTIONARY_API_KEY',
                            'OMNISHOTS_NAMETHETUNE_CLIENT_ID',
                            'OMNISHOTS_NAMETHETUNE_CLIENT_SECRET',
                        ]),
                    }
                )

                const integration = new apigateway.LambdaIntegration(handler, {
                    requestTemplates: { 'application/json': '{ "statusCode": "200" }' }
                })

                resource.addMethod(method.method, integration)
            }
        }
    }
}

new OmnishotsStack(new App(), 'OmnishotsStack')
