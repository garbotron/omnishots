export interface Logger {
    debug(s: string, ...data: any[]): void,
    info(s: string, ...data: any[]): void,
    warn(s: string, ...data: any[]): void,
    error(s: string, ...data: any[]): void,
}

enum LoggerLevel {
    Debug,
    Info,
    Warn,
    Error,
}

let globalLoggerLevel = LoggerLevel.Info

class LoggerImpl implements Logger {
    private name: string

    constructor(name: string) {
        this.name = name
    }

    public debug(s: string, ...data: any[]) {
        this.print(LoggerLevel.Debug, console.log, s, data)
    }

    public info(s: string, ...data: any[]) {
        this.print(LoggerLevel.Info, console.log, s, data)
    }

    public warn(s: string, ...data: any[]) {
        this.print(LoggerLevel.Warn, console.log, s, data)
    }

    public error(s: string, ...data: any[]) {
        this.print(LoggerLevel.Error, console.error, s, data)
    }

    private print(level: LoggerLevel, func: (s: string, ...data: any[]) => void, s: string, data: any[]) {
        if (globalLoggerLevel <= level) {
            func(`[${this.name}] [${LoggerLevel[level].toLowerCase()}] ${s}`, ...data)
        }
    }
}

export function getLogger(name: string): Logger {
    return new LoggerImpl(name)
}

export function enableAllDebugLogs() {
    globalLoggerLevel = LoggerLevel.Debug
}
