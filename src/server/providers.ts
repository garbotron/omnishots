import * as common from './common.js'
import Animeclips from './providers/animeclips.js'
import Animeshots from './providers/animeshots.js'
import Dictionary from './providers/dictionary.js'
import Dummy from './providers/dummy.js'
import Gamershots from './providers/gamershots.js'
import Jeopardy from './providers/jeopardy.js'
import Movieshots from './providers/movieshots.js'
import Mtg from './providers/mtg.js'
import NameTheTune from './providers/namethetune.js'
import Streetview from './providers/streetview.js'

const providers: common.Provider[] = [
    new Gamershots(),
    new Animeshots(),
    new Animeclips(),
    new Movieshots(),
    new NameTheTune(),
    new Jeopardy(),
    new Streetview(),
    new Dictionary(),
    new Mtg(),
    new Dummy(),
]

export default function getAllProviders(): common.Provider[] {
    return providers
}
