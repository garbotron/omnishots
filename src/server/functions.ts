import * as lambda from 'aws-lambda'
import * as model from '../common/model.js'
import * as common from './common.js'
import * as serverData from './server-data.js'
import providers from './providers.js'

export interface OmnishotsServerFunction {
    name: string,
    methods: {
        method: string,
        handlerName: string,
        handler: (event: lambda.APIGatewayProxyEvent, context: lambda.Context) => Promise<lambda.APIGatewayProxyResult>,
    }[]
}

export function getAllServerFunctions(): OmnishotsServerFunction[] {
    return [
        { name: 'providers', methods: [{ method: 'GET', handlerName: 'providersGet', handler: providersGet }] },
        { name: 'content', methods: [{ method: 'POST', handlerName: 'contentGet', handler: contentGet }] },
        { name: 'test', methods: [{ method: 'POST', handlerName: 'filterTest', handler: filterTest }] },
        { name: 'presets', methods: [
            { method: 'GET', handlerName: 'presetsGet', handler: presetsGet },
            { method: 'POST', handlerName: 'presetsSet', handler: presetsSet },
        ]},
    ]
}

export async function providersGet(event: lambda.APIGatewayProxyEvent, _: lambda.Context) {
    return common.run('providers get', event, run)

    function run(__: unknown): Promise<model.ProviderReport> {
        const dbVars = providers().map((x) => x.getDbUriEnvironmentVariableName()).filter((x) => x)
        return Promise.all(dbVars.map((x) => common.openDb(x!)))
            .then(() => Promise.all(providers().map((x) => x.getInfo())))
            .then<model.ProviderReport>((x) => ({ providers: x.sort((a, b) => a.name > b.name ? 1 : -1) }))
    }
}

export async function contentGet(event: lambda.APIGatewayProxyEvent, _: lambda.Context) {
    return common.run('content get', event, run)

    function run(input: model.ContentQuery): Promise<model.Content> {
        return validateInput(input)
            .then(findProvider)
            .then(openDb)
            .then(generateContent)
    }

    function validateInput(input: model.ContentQuery): Promise<model.ContentQuery> {
        if (input.providerId === undefined) {
            return Promise.reject({ code: 400, message: '"providerId" parameter not specified' })
        } else if (input.filterValues === undefined) {
            return Promise.reject({ code: 400, message: '"filterValues" parameter not specified' })
        } else {
            return Promise.resolve(input)
        }
    }

    function findProvider(input: model.ContentQuery): Promise<[model.ContentQuery, common.Provider]> {
        const p = providers().find((x) => x.id === input.providerId)
        if (p) {
            return Promise.resolve<[model.ContentQuery, common.Provider]>([input, p])
        } else {
            return Promise.reject({ code: 404, message: 'provider not found' })
        }
    }

    function openDb(input: [model.ContentQuery, common.Provider]): Promise<[model.ContentQuery, common.Provider]> {
        const dbVar = input[1].getDbUriEnvironmentVariableName()
        if (dbVar) {
            return common.openDb(dbVar).then(() => input)
        } else {
            return Promise.resolve(input)
        }
    }

    function generateContent(input: [model.ContentQuery, common.Provider]): Promise<model.Content> {
        const [query, p] = input
        return p.produceContent(query.filterValues)
    }
}

export async function filterTest(event: lambda.APIGatewayProxyEvent, _: lambda.Context) {
    return common.run('filter test', event, run)

    function run(input: model.ContentQuery): Promise<model.FilterTestResult> {
        return validateInput(input)
            .then(findProvider)
            .then(openDb)
            .then(checkFilters)
    }

    function validateInput(input: model.ContentQuery): Promise<model.ContentQuery> {
        if (input.providerId === undefined) {
            return Promise.reject({ code: 400, message: '"providerId" parameter not specified' })
        } else if (input.filterValues === undefined) {
            return Promise.reject({ code: 400, message: '"filterValues" parameter not specified' })
        } else {
            return Promise.resolve(input)
        }
    }

    function findProvider(input: model.ContentQuery): Promise<[model.ContentQuery, common.Provider]> {
        const p = providers().find((x) => x.id === input.providerId)
        if (p) {
            return Promise.resolve<[model.ContentQuery, common.Provider]>([input, p])
        } else {
            return Promise.reject({ code: 404, message: 'provider not found' })
        }
    }

    function openDb(input: [model.ContentQuery, common.Provider]): Promise<[model.ContentQuery, common.Provider]> {
        const dbVar = input[1].getDbUriEnvironmentVariableName()
        if (dbVar) {
            return common.openDb(dbVar).then(() => input)
        } else {
            return Promise.resolve(input)
        }
    }

    function checkFilters(input: [model.ContentQuery, common.Provider]): Promise<model.FilterTestResult> {
        const [query, p] = input
        return p.checkFilters(query.filterValues)
    }
}

export async function presetsGet(event: lambda.APIGatewayProxyEvent, _: lambda.Context) {
    return common.run('presets get', event, run)

    function run(__: unknown): Promise<model.FilterPresets> {
        return serverData.init().then(() => serverData.getPresets()).then((x) => ({ presets: x }))
    }
}

export async function presetsSet(event: lambda.APIGatewayProxyEvent, _: lambda.Context) {
    return common.run('presets get', event, run)

    function run(input: model.FilterPresets): Promise<unknown> {
        return validateInput(input).then(setPresets)
    }

    function validateInput(input: model.FilterPresets): Promise<model.FilterPresets> {
        if (input.presets === undefined) {
            return Promise.reject({ code: 400, message: '"presets" parameter not specified' })
        } else {
            return Promise.resolve(input)
        }
    }

    function setPresets(input: model.FilterPresets): Promise<unknown> {
        return serverData.init().then(() => serverData.setPresets(input.presets)).then(() => ({}))
    }
}
