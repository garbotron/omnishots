import * as fs from 'fs'
import * as common from './common.js'
import * as logger from './logger.js'
import providers from './providers.js'

process.on('warning', (e: any) => console.warn(e.stack))
process.on('uncaughtException', (e: any) => console.error(e.stack))
process.on('unhandledRejection', (e: any) => console.error(e.stack))

function run() {
    const mainLog = logger.getLogger('main')
    mainLog.info('Scraper build function started')

    if (process.argv.length < 3) {
        mainLog.error(`Please specify a provider - valid entries are: ${listProviders()}`)
        process.exit(1)
    }

    const id = process.argv[2]
    const provider = providers().find((x) => x.id === id)
    if (!provider) {
        mainLog.error(`Provider "${id}" not found - valid entries are: ${listProviders()}`)
        process.exit(1)
    }

    const scraper = provider.getScraper()
    if (!scraper) {
        mainLog.error(`Provider "${id}" does not have an associated scraper`)
        process.exit(1)
    }

    const scraperLog = logger.getLogger(`scraper.${id}`)
    mainLog.info(`Starting scraper "${id}"`)

    const localDbPath = common.localDbFilePath(provider.id)
    mainLog.info(`Local DB file: "${localDbPath}"`)

    let localDb: common.LocalDb = { collections: [] }
    if (fs.existsSync(localDbPath)) {
        localDb = JSON.parse(fs.readFileSync(localDbPath, 'utf8'))
    }

    const context = { log: scraperLog, localDb, localDbPath };
    scraper.fillLocalDb(context)
        .then(() => common.saveLocalDb(context))
        .then(() => mainLog.info(`Local DB file "${localDbPath}" created successfully`))
        .catch((err) => {
            if (err instanceof Error) {
                mainLog.error(`Scrape aborted due to error`, err)
            } else {
                mainLog.error(`Scrape aborted due to unexpected error: ${JSON.stringify(err)}`)
            }
        })

    function listProviders() {
        return providers()
            .map((x) => ({ p: x.id, s: x.getScraper() }))
            .filter((x) => x.s)
            .map((x) => x.p)
            .join(', ')
    }
}

run()
