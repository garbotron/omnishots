import * as common from './common.js'
import * as model from '../common/model.js'

export const DB_URI_ENV = 'OMNISHOTS_CORE_MONGODB_URI'
export const DB_COLLECTION = 'collection'

export async function init(): Promise<void> {
    return common.openDb(DB_URI_ENV)
}

export function getPresets(): Promise<model.FilterPreset[]> {
    return common.db(DB_URI_ENV, DB_COLLECTION).then((db) => {
        return db.findOne({ kind: 'presets' })
    }).then((rec) => {
        return rec?.presets || []
    })
}

export function setPresets(presets: model.FilterPreset[]): Promise<void> {
    return common.db(DB_URI_ENV, DB_COLLECTION).then((db) => {
        return db.findOneAndReplace({ kind: 'presets' }, { kind: 'presets', presets }, { upsert: true })
    }).then(() => { return })
}
