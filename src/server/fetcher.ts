// A simple service to make it easier to perform lots of HTTP requests:
// - Enforce a minimum time between fetches.
// - Allow multiple fetches to be queued at once.
// - Retry multiple times before giving up.

import * as common from './common.js'
import * as logger from './logger.js'

function delay(ms: number): Promise<void> {
    return new Promise<void>((resolve) => setTimeout(resolve, ms))
}

export default class Fetcher {
    public timeBetweenRequestsMs = 1500
    public retryBackoffMs = 1500
    public maxRetries = 10

    private readonly log = logger.getLogger('fetcher')
    private readonly waitingForATurn: (() => void)[] = []
    private active = false
    private lastSendTimeMs = 0

    public constructor(init?: Partial<Fetcher>) {
        Object.assign(this, init)
    }

    public fetch(uri: string, expectedContent: string): Promise<string> {
        const wait = new Promise<void>((resolve) => {
            if (this.active) {
                // Wait for our turn.
                this.waitingForATurn.push(resolve)
            } else {
                this.active = true
                resolve()
            }
        })
        return wait.then(() => this.get(uri, expectedContent, this.maxRetries))
    }

    private get(uri: string, expectedContent: string, retriesRemaining: number): Promise<string> {
        // If we've sent too recently, wait for a moment and try again.
        const nowMs = Date.now()
        if (nowMs - this.lastSendTimeMs < this.timeBetweenRequestsMs) {
            const toWaitMs = this.timeBetweenRequestsMs - (nowMs - this.lastSendTimeMs)
            return delay(toWaitMs).then(() => this.get(uri, expectedContent, retriesRemaining))
        }

        this.lastSendTimeMs = nowMs
        return common.get(uri).then((content) => {
            if (!expectedContent || content.indexOf(expectedContent) >= 0) {
                this.log.debug(`success: ${uri}`)
                this.active = false
                if (this.waitingForATurn.length > 0) {
                    this.waitingForATurn[0]()
                    this.waitingForATurn.splice(0, 1)
                }
                return Promise.resolve(content)
            } else {
                this.log.info(`content for URI '${uri}' didn't look right`)
                this.log.info(content)
                process.exit(1)
                return this.retryOrFail(uri, expectedContent, retriesRemaining)
            }
        }, (err) => {
            this.log.info(`fetch internal error: ${err}`)
            return this.retryOrFail(uri, expectedContent, retriesRemaining)
        })
    }

    private retryOrFail(uri: string, expectedContent: string, retriesRemaining: number): Promise<string> {
        if (retriesRemaining === 0) {
            this.log.debug(`gave up: ${uri}`)
            return Promise.reject(new Error(`Retried ${this.maxRetries} times and still failed`))
        } else {
            this.log.debug(`retry: ${uri} (${retriesRemaining} remaining)`)
            return delay(this.retryBackoffMs).then(() => this.get(uri, expectedContent, retriesRemaining - 1))
        }
    }
}
