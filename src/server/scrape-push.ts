import * as fs from 'fs'
import * as common from './common.js'
import * as logger from './logger.js'
import providers from './providers.js'

process.on('warning', (e: any) => console.warn(e.stack))
process.on('uncaughtException', (e: any) => console.error(e.stack))
process.on('unhandledRejection', (e: any) => console.error(e.stack))

function run() {
    const mainLog = logger.getLogger('main')
    mainLog.info('Scraper push function started')

    if (process.argv.length < 3) {
        mainLog.error(`Please specify a provider - valid entries are: ${listProviders()}`)
        process.exit(1)
    }

    const id = process.argv[2]
    const provider = providers().find((x) => x.id === id)
    if (!provider) {
        mainLog.error(`Provider "${id}" not found - valid entries are: ${listProviders()}`)
        process.exit(1)
    }

    const scraper = provider.getScraper()
    if (!scraper) {
        mainLog.error(`Provider "${id}" does not have an associated scraper`)
        process.exit(1)
    }

    mainLog.info(`Starting scraper "${id}"`)

    const dbVar = provider.getDbUriEnvironmentVariableName()
    if (!dbVar) {
        mainLog.error('getDbUriEnvironmentVariableName() not implemented')
        process.exit(1)
    }

    const scraperLog = logger.getLogger(`scraper.${id}`)
    mainLog.info(`Starting scraper "${id}"`)

    const localDbPath = common.localDbFilePath(provider.id)
    mainLog.info(`Local DB file: "${localDbPath}"`)

    if (!fs.existsSync(localDbPath)) {
        mainLog.error(`${localDbPath} doesn't exist. Run scrape-build first to create the file.`)
        process.exit(1)
    }

    const localDb = JSON.parse(fs.readFileSync(localDbPath, 'utf8'))

    common.openDb(dbVar)
        .then(() => scraper.prepareLocalDbForUpload({ log: scraperLog, localDb, localDbPath }))
        .then(() => common.pushLocalDb(dbVar, localDb))
        .then(() => common.closeDbs())
        .then(() => mainLog.info(`Scraper "${id}" data pushed successfully`))
        .catch((err) => {
            if (err instanceof Error) {
                mainLog.error(`Push aborted due to error`, err)
            } else {
                mainLog.error(`Push aborted due to unexpected error: ${JSON.stringify(err)}`)
            }
            common.closeDbs()
        })

    function listProviders() {
        return providers()
            .map((x) => ({ p: x.id, s: x.getScraper() }))
            .filter((x) => x.s)
            .map((x) => x.p)
            .join(', ')
    }
}

run()
