import * as mongo from 'mongodb'
import * as common from '../common.js'

export const DB_URI_ENV = 'OMNISHOTS_JEOPARDY_MONGODB_URI'
export const DB_COLLECTION = 'collection'

export type Level = number | 'FINAL'

export interface Question {
    year: number,
    category: string,
    level: Level,
    clue: string,
    answer: string,
    url: string,
}

export async function find(query?: mongo.Filter<Question>): Promise<Question | 'NOT_FOUND'> {
    const db = await common.db(DB_URI_ENV, DB_COLLECTION)
    const numRecs = await db.countDocuments(query || {})
    const idx = Math.floor(Math.random() * numRecs)
    const result = await db.find<Question>(query || {}).limit(-1).skip(idx).next()
    return result || 'NOT_FOUND'
}

export async function count(query?: mongo.Filter<Question>): Promise<number> {
    const db = await common.db(DB_URI_ENV, DB_COLLECTION)
    return await db.countDocuments(query || {})
}
