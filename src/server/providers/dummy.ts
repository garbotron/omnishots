import * as model from '../../common/model.js'
import * as common from '../common.js'

export default class Provider implements common.Provider {
    public readonly id = 'dummy'

    public getInfo(): Promise<model.ProviderInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Dummy',
            title: 'DummyShots: Fill in your game here',
            prompt: 'Play your custom game now',
            elemDetailsTitle: 'Go to google for no reason',
            description: [
                'DummyShots is a placeholder to allow you to play your own game in the Omnishots engine.',
                'When prompted, just play your own game!',
                '<b>If you lose, take a drink!</b>',
                '<b>If you win, everyone else takes a drink and you go again!</b>',
            ],
            filters: [],
        })
    }

    public checkFilters(_: model.FilterValue[]): Promise<model.FilterTestResult> {
        return Promise.resolve<model.FilterTestResult>({ filteredCount: 'UNKNOWN', totalCount: 'UNKNOWN' })
    }

    public produceContent(_: model.FilterValue[]): Promise<model.Content> {
        const content = '<div style="width:100%;height:100%;display:flex;justify-content:center;">' +
            '<div style="align-self:center;font-size:600%;color:white;">' +
            `<strong>Go play your own game now...</strong>` +
            '</div>'
        return Promise.resolve<model.Content>({
            result: 'FOUND',
            content,
            solution: 'Hope you won!',
            detailsUrl: 'http://www.google.com',
        })
    }

    public getDbUriEnvironmentVariableName() {
        return undefined
    }

    public getScraper() {
        return undefined
    }
}
