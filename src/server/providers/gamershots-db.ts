import * as mongo from 'mongodb'
import * as common from '../common.js'

export const DB_URI_ENV = 'OMNISHOTS_GAMES_MONGODB_URI'
export const DB_COLLECTION = 'collection'

export interface Game {
    shortName: string
    name: string
    releaseDate: number
    numReviews: number
    averageReviewScore: number
    screenshotUrls: string[]
    primarySystems: string[]
    rereleaseSystems: string[]
    genres: string[]
    themes: string[]
    regions: string[]
}

function findInternal(db: mongo.Collection, query?: mongo.Filter<Game>): Promise<Game | 'NOT_FOUND'> {
    return db.countDocuments(query || {}).then((numRecs) => {
        const idx = Math.floor(Math.random() * numRecs)
        return db.find<Game>(query || {}).limit(-1).skip(idx).next().then((x) => x || 'NOT_FOUND')
    })
}

export function find(query?: mongo.Filter<Game>): Promise<Game | 'NOT_FOUND'> {
    return common.db(DB_URI_ENV, DB_COLLECTION).then((x) => findInternal(x, query))
}

export function count(query?: mongo.Filter<Game>): Promise<number> {
    return common.db(DB_URI_ENV, DB_COLLECTION).then((x) => x.countDocuments(query || {}))
}

function getAllUniqueFromLists(property: string): Promise<string[]> {
    property = '$' + property
    const pipeline = [
        { $unwind: property },
        { $group: { _id: property } },
        { $sort: { _id: 1 } },
    ]
    return common.db(DB_URI_ENV, DB_COLLECTION)
        .then((x) => x.aggregate<{ _id: string }>(pipeline))
        .then((x) => x.toArray())
        .then((x) => x.map((y) => y._id))
}

export function getAllSystems(): Promise<string[]> {
    return getAllUniqueFromLists('primarySystems')
}

export function getAllGenres(): Promise<string[]> {
    return getAllUniqueFromLists('genres')
}

export function getAllThemes(): Promise<string[]> {
    return getAllUniqueFromLists('themes')
}
