import * as he from 'he'
import * as model from '../../common/model.js'
import * as common from '../common.js'
import * as db from './jeopardy-db.js'
import Scraper from './jeopardy-scraper.js'

interface Filter {
    getInfo(): model.FilterInfo
    getFilterComponent(vals: number[]): any // undefined or match component like { field: { $gte: 42 } }
}

class FilterDifficulty implements Filter {
    public getInfo() {
        return {
            id: 'difficulty',
            name: 'Filter by Difficulty',
            prompt: 'Clue difficulty 1-5 (6 for Final Jeopardy!)',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [1, 5],
        }
    }

    public getFilterComponent(vals: number[]): any {
        if (vals.length < 2) {
            return undefined // no filter
        }

        // In the filter, we use [1..5] for the rounds and 6 for Final Jeopardy! - in the DB we use 1..5 and 'FINAL'
        const allowedValues: db.Level[] = []
        for (let i = vals[0]; i <= Math.min(vals[1], 5); i++) {
            allowedValues.push(i)
        }
        if (vals[1] >= 6) {
            allowedValues.push('FINAL')
        }
        return { level: { $in: allowedValues } }
    }
}

class FilterYear implements Filter {
    public getInfo() {
        return {
            id: 'year',
            name: 'Filter by Year',
            prompt: 'Year when episode aired',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [1984, new Date().getFullYear()],
        }
    }

    public getFilterComponent(vals: number[]): any {
        if (vals.length < 2) {
            return undefined // no filter
        }
        return { $and: [{ year: { $gte: vals[0] } }, { year: { $lte: vals[1] } }] }
    }
}

const filters: Filter[] = [
    new FilterDifficulty(),
    new FilterYear(),
]

export default class Provider implements common.Provider {
    public readonly id = 'jeopardy'

    public getInfo(): Promise<model.ProviderInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Jeopardy!',
            title: 'Jeopardyshots! Answer in the Form of a Question!',
            prompt: 'Answer in the form of a question.',
            elemDetailsTitle: 'Episode Info',
            description: [
                'Jeopardyshots is a drinking game for trivia geeks.',
                'Can you get a Jeopardy! question right from all of the show\'s history?',
                '<b>If you can\'t get it, take a drink!</b>',
                '<b>If you can, everyone else takes a drink and you go again!</b>',
            ],
            filters: filters.map((x) => x.getInfo()),
        })
    }

    public checkFilters(filterValues: model.FilterValue[]): Promise<model.FilterTestResult> {
        return db.count(this.getQuery(filterValues)).then((filteredCount) => {
            return db.count().then((totalCount) => ({ filteredCount, totalCount }))
        })
    }

    public produceContent(filterValues: model.FilterValue[]): Promise<model.Content> {
        return db.find(this.getQuery(filterValues)).then<model.Content>((x) => {
            if (x === 'NOT_FOUND') {
                return { result: 'NOT_FOUND' }
            } else {
                return {
                    result: 'FOUND',
                    content: this.formatContent(x),
                    solution: he.encode(x.answer),
                    detailsUrl: x.url,
                }
            }
        })
    }

    public getDbUriEnvironmentVariableName() {
        return db.DB_URI_ENV
    }

    public getScraper(): common.Scraper | undefined {
        return new Scraper()
    }

    private formatContent(q: db.Question): string {
        return '<div style="width:100%;height:100%;background-color:#0A1981;display:flex;justify-content:center;">' +
            '<div style="align-self:center;margin:5em;font-family:Serif;color:white;font-size:350%;' +
            'line-height:1em;text-shadow:0.05em 0.05em 0.02em #111;">' +
            `<strong>${he.encode(q.category)}</strong>` +
            `<br>${q.year} - ${q.level === 'FINAL' ? 'Final Jeopardy!' : `Difficulty ${q.level}`}` +
            `<br><br><br>${he.encode(q.clue)}</div></div>`
    }

    private getQuery(filterValues: model.FilterValue[]): object | undefined {
        const matchFilters: any[] = []
        for (const filterValue of filterValues) {
            const filter = filters.find((x) => x.getInfo().id === filterValue.filterId)
            if (filter) {
                const component = filter.getFilterComponent(filterValue.values)
                if (component) {
                    matchFilters.push(component)
                }
            }
        }
        return matchFilters.length > 0 ? { $and: matchFilters } : undefined
    }
}
