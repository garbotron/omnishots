// This old scaper doesn't work anymore, but the new one (gamershots-scraper-wip.txt) is too slow.
// Gamershots has no scraper for now.

// import { CheerioAPI, Element, load } from 'cheerio'
// import * as he from 'he'
// import * as common from '../common.js'
// import Fetcher from '../fetcher.js'
// import * as db from './gamershots-db.js'

// const testMode = false // if true, we only scrape a single page of a single year
// const gamesPerListingsPage = 25
// const maxScreenshotsPerGame = 50
// const firstYearWithListings = 1972
// const gamesToAvoidScraping = [
//     'shot', // for whatever reason, this game gives a 404 error (even though it's in the normal listing)
// ]

// interface Context extends common.ScraperContext {
//     cancelled: boolean
//     gameCount: number
//     readonly fetcher: Fetcher
// }

// // download a webpage via HTTP GET using a proxy and parse it with cheerio
// function fetch(context: Context, url: string): Promise<CheerioAPI> {
//     if (context.cancelled) {
//         return Promise.reject(new Error('scrape was cancelled'))
//     }
//     url = rootUrl(url)
//     // Use the Patreon link to determine page validity.
//     return context.fetcher.fetch(url, 'https://www.patreon.com/mobygames')
//         .then((x) => {
//             if (context.cancelled) {
//                 return Promise.reject(new Error('scrape was cancelled'))
//             }
//             return Promise.resolve(x)
//         })
//         .then((x) => load(x))
// }

// function rootUrl(page: string): string {
//     if (!page.toLowerCase().startsWith('https://')) {
//         if (!page.startsWith('/')) {
//             page = `/${page}`
//         }
//         page = `https://www.mobygames.com${page}`
//     }
//     return page
// }

// function scrapeGameMain(context: Context, shortName: string): Promise<Partial<db.Game>> {
//     return fetch(context, `https://www.mobygames.com/game/${shortName}/`).then<Partial<db.Game>>(($) => {
//         const name = he.decode($('h1.niceHeaderTitle>a').first().text()).trim()
//         if (!name) {
//             return Promise.reject(new Error(`'${shortName}': long name not found`))
//         }
//         let genres: string[] = []
//         let themes: string[] = []
//         for (const div of common.split($('#coreGameGenre div'))) {
//             const text = div.text()
//             if (['Genre', 'Genres'].some((x) => x === text)) {
//                 genres = genres.concat(common.split(div.next().find('a')).map((x) => x.text()))
//             }
//             // include perspectives and misc items under the themes category
//             if (['Theme', 'Themes', 'Misc', 'Perspective', 'Perspectives'].some((x) => x === text)) {
//                 themes = themes.concat(common.split(div.next().find('a')).map((x) => x.text()))
//             }
//         }
//         return { name, genres, themes }
//     })
// }

// function scrapeGameRank(context: Context, shortName: string): Promise<Partial<db.Game>> {
//     return fetch(context, `https://www.mobygames.com/game/${shortName}/mobyrank`).then<Partial<db.Game>>(($) => {
//         let reviewTotal = 0
//         let numReviews = 0
//         for (const div of common.split($('div.fl.scoreBoxMed'))) {
//             const reviewScore = parseInt(div.text(), undefined)
//             if (isNaN(reviewScore)) {
//                 // this happens sometimes when the review doesn't actually have an associated score
//                 continue
//             }
//             numReviews++
//             reviewTotal += reviewScore
//         }
//         const averageReviewScore = numReviews ? reviewTotal / numReviews : 0
//         return { averageReviewScore, numReviews }
//     })
// }

// function scrapeGameReleases(context: Context, shortName: string): Promise<Partial<db.Game>> {
//     return fetch(context, `https://www.mobygames.com/game/${shortName}/release-info`).then<Partial<db.Game>>(($) => {
//         let regions: string[] = []
//         const systemYears: { system: string, year: number }[] = []
//         for (const countryTitle of common.split($('div.relInfoTitle:contains("Countr")'))) {
//             const countries: string[] = []
//             const countryHolder = countryTitle.parent()
//             for (const countrySpan of common.split(countryHolder.find('div.relInfoDetails span'))) {
//                 const newCountry = countrySpan.text().replace(/^[ ,]+|[ ,]+$/g, '') // trim spaces and commas
//                 if (newCountry) {
//                     countries.push(newCountry)
//                 }
//             }
//             if (countries.length === 0) {
//                 // sometimes the country is blank and we hould just ignore these cases
//                 continue
//             }
//             regions = regions.concat(countries)

//             const system = countryHolder.parent().prevAll('h2').first().text() // for some reason, 'prev()' doesn't work
//             if (!system) {
//                 return Promise.reject(new Error(`'${shortName}': could not find system h2`))
//             }
//             if (systemYears.find((x) => system === x.system)) {
//                 continue
//             }

//             const dateHolder = countryHolder.next()
//             const dateDiv = dateHolder.find('div.relInfoDetails')
//             const date = dateDiv.text().replace(/^(.*, *)?/, '') // trim everything up through the comma if there is one
//             const year = parseInt(date, undefined)
//             if (isNaN(year)) {
//                 return Promise.reject(new Error(`'${shortName}': invalid date`))
//             }
//             systemYears.push({ system, year })
//         }

//         const releaseDate = regions.length === 0 ? -1 : Math.min(...systemYears.map((x) => x.year))
//         const primarySystems = systemYears.filter((x) => x.year - releaseDate <= 2).map((x) => x.system)
//         const rereleaseSystems = systemYears.filter((x) => x.year - releaseDate > 2).map((x) => x.system)
//         return { regions, releaseDate, primarySystems, rereleaseSystems }
//     })
// }

// function scrapeGameScreenshots(context: Context, shortName: string): Promise<Partial<db.Game>> {
//     return fetch(context, `https://www.mobygames.com/game/${shortName}/screenshots`).then<Partial<db.Game>>(($) => {
//         let urls: string[] = []

//         // Add all user-submitted screenshots.
//         for (const outer of common.split($('div.thumbnail'))) {
//             const allTxt = outer.text().toLowerCase()
//             if (!allTxt.includes('title') || !allTxt.includes('main menu')) {
//                 const match = /background-image:url\((\/images\/shots\/[^)]+)\)/.exec(outer.html()!)
//                 if (match) {
//                     urls.push(rootUrl(match[1]))
//                 }
//             }
//         }

//         // Add all promo/official screenshots (but prefer the user screenshots).
//         for (const href of common.split($('ul.thumbnailGallery img')).map((x) => x.attr('src'))) {
//             if (href && href.startsWith('/images/promo/') && urls.length < maxScreenshotsPerGame) {
//                 urls.push(rootUrl(href))
//             }
//         }

//         // Change the 's' subdirectory (small images) to 'l' (large images).
//         urls = urls.map((x) => x.replace('/s/', '/l/'))

//         // Filter out screenshot URLs that won't fit in the DB table.
//         urls = urls.filter((x) => x.length <= 255)

//         // Randomly filter the set of screenshot URLs down to the maximum size.
//         while (urls.length > maxScreenshotsPerGame) {
//             const idx = Math.floor(Math.random() * urls.length)
//             urls.splice(idx, 1)
//         }

//         // Trim the common prefix from every screenshot that has it (to save space).
//         urls = urls.map((x) => x.replace('https://www.mobygames.com/images', ''))

//         return { screenshotUrls: urls }
//     })
// }

// function scrapeGame(context: Context, shortName: string): Promise<void> {
//     context.log.info(`scraping game #${context.gameCount}: '${shortName}'`)

//     // Scrape screenshots first, so we can abort early when there are 0 screenshots.
//     const game: Partial<db.Game> = { shortName }
//     return scrapeGameScreenshots(context, shortName)
//         .then((p) => {
//             if (!p.screenshotUrls || p.screenshotUrls.length === 0) {
//                 context.log.info(`skipping due to no screenshots found: '${shortName}'`)
//                 return Promise.resolve()
//             }

//             Object.assign(game, p)
//             return scrapeGameMain(context, shortName)
//                 .then((x) => { Object.assign(game, x) })
//                 .then(() => scrapeGameRank(context, shortName))
//                 .then((x) => { Object.assign(game, x) })
//                 .then(() => scrapeGameReleases(context, shortName))
//                 .then((x) => { Object.assign(game, x) })
//                 .then(() => {
//                     // remove duplicates
//                     game.screenshotUrls = Array.from(new Set(game.screenshotUrls))
//                     game.primarySystems = Array.from(new Set(game.primarySystems))
//                     game.rereleaseSystems = Array.from(new Set(game.rereleaseSystems))
//                     game.genres = Array.from(new Set(game.genres))
//                     game.themes = Array.from(new Set(game.themes))
//                     game.regions = Array.from(new Set(game.regions))

//                     if (game.regions.length === 0) {
//                         context.log.info(`skipping due to no regions found: '${game.name}'`)
//                         return Promise.resolve()
//                     }

//                     context.log.info(`scrape complete: '${game.name}' (${game.screenshotUrls.length} screenshots)`)
//                     context.gameCount++
//                     return db.addToTemp(game as db.Game)
//                 })
//         })
// }

// function scrapeOrDiscardGame(context: Context, shortName: string): Promise<void> {
//     if (gamesToAvoidScraping.indexOf(shortName) >= 0) {
//         return Promise.resolve()
//     }
//     return db.findTemp(shortName).then((x) => {
//         if (x === 'NOT_FOUND') {
//             return scrapeGame(context, shortName)
//         } else {
//             context.log.info(`discarding game #${context.gameCount}: '${shortName}' - already scraped`)
//             return Promise.resolve()
//         }
//     })
// }

// function scrapeGameListings(context: Context, year: number, offset: number): Promise<void> {
//     const url = `https://www.mobygames.com/browse/games/${year}/offset,${offset}/list-games/`
//     const games: string[] = []
//     return fetch(context, url).then(($) => {
//         context.log.info(`Scraping year: ${year}, offset ${offset}`)
//         $('#mof_object_list a').each((_: number, link: Element) => {
//             const href = link.attribs.href
//             if (href && href.startsWith('https://www.mobygames.com/game/')) {
//                 const name = href.substr('https://www.mobygames.com/game/'.length)
//                 context.log.info(`found '${name}'`)
//                 games.push(name)
//             }
//         })

//         // scrape each game in sequence
//         let promise = Promise.resolve()
//         for (const g of games) {
//             promise = promise.then(() => scrapeOrDiscardGame(context, g))
//         }

//         // if we're not on the last page, continue with the next one
//         if (games.length === gamesPerListingsPage && !testMode) {
//             promise = promise.then(() => scrapeGameListings(context, year, offset + gamesPerListingsPage))
//         }

//         return promise
//     })
// }

// function scrapeAllGameListings(context: Context): Promise<void> {
//     // create 5 separate promises so we handle the scraping in 5 "batches"
//     const promises = Array<Promise<void>>(5).fill(Promise.resolve())
//     let curPromise = 0
//     for (let year = new Date().getFullYear(); year >= firstYearWithListings; year--) {
//         if (testMode && year !== 1994) {
//             continue
//         }
//         promises[curPromise] = promises[curPromise].then(() => scrapeGameListings(context, year, 0))
//         curPromise = curPromise === promises.length - 1 ? 0 : curPromise + 1
//     }
//     return Promise.all(promises).then(() => { return }).catch((reason) => {
//         context.cancelled = true
//         return Promise.reject(reason)
//     })
// }

// function scrape(context: Context): Promise<void> {
//     context.log.info('Prepping DB')
//     return db.prepareTemp()
//         .then(() => scrapeAllGameListings(context))
//         .then(() => {
//             context.log.info('Scrape complete, committing DB')
//             if (testMode) {
//                 return Promise.resolve()
//             } else {
//                 return db.commitTemp()
//             }
//         })
// }

// export default class Scraper implements common.Scraper {
//     public go(context: common.ScraperContext): Promise<void> {
//         return scrape({
//             log: context.log,
//             cancelled: false,
//             gameCount: 0,
//             fetcher: new Fetcher({ retryBackoffMs: 60000, maxRetries: 180 }),
//         })
//     }
// }
