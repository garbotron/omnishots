import * as model from '../../common/model.js'
import * as common from '../common.js'
import * as logger from '../logger.js'

interface RandomLocationsJson {
    success: boolean,
    locations: {
        lat: string,
        lng: string,
        formatted_address: string,
    }[]
}

interface ReverseGeocoderJson {
    display_name: string
}

const log = logger.getLogger('streetview')

function queryRandomLocations(): Promise<RandomLocationsJson> {
    const form = { country: 'all' }
    return common.post('https://randomstreetview.com/data', { form })
}

function queryReverseGeocoder(lat: number, lng: number): Promise<ReverseGeocoderJson> {
    const email = 'omnishots@garbotron.gitlab.io'
    const url = 'https://nominatim.openstreetmap.org/reverse?'
        + `format=json&zoom=6&accept-language=en&lat=${lat}&lon=${lng}&email=${email}`
    return common.getJson<ReverseGeocoderJson>(url).then((data) => {
        if (!data.display_name) {
            return Promise.reject('geocoding data was improperly formatted')
        } else {
            return data
        }
    })
}

function getRandomUsableCoordinates(): Promise<{ lat: number, lng: number }> {
    return queryRandomLocations().then((x) => {
        if (!x.success) {
            return Promise.reject('Failure to query random location')
        } else {
            return { lat: +x.locations[0].lat, lng: +x.locations[0].lng }
        }
    })
}

export default class Provider implements common.Provider {
    public readonly id = 'streetview'

    public getInfo(): Promise<model.ProviderInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'StreetView',
            title: 'StreetView: Name That Location!',
            prompt: 'Where is this?',
            elemDetailsTitle: 'Go to Google Maps',
            description: [
                'StreeView is a drinking game for geography dorks.',
                'Check out a random streetview panorama, see if you can name the location!',
                '<b>If you can\'t name the location, take a drink!</b>',
                '<b>If you can, everyone else takes a drink and you go again!</b>',
            ],
            filters: [],
        })
    }

    public checkFilters(_: model.FilterValue[]): Promise<model.FilterTestResult> {
        return Promise.resolve<model.FilterTestResult>({ filteredCount: 'UNKNOWN', totalCount: 'UNKNOWN' })
    }

    public produceContent(_: model.FilterValue[]): Promise<model.Content> {
        // The procedure is like this:
        // 1. Pick a random latitude/longitude
        // 2. Query Google StreetView for a nearby street
        // 3. If it comes back with nothing (such as over the ocean), go back to #1
        // 4. Query Google geocoding with the selected coordinates to get the name of the place (and country, etc)
        return getRandomUsableCoordinates()
            .then(getGeocodingData)
            .then(getLocationData)
            .then(getContent)

        interface GeoData {
            lat: number,
            lng: number,
            geo: ReverseGeocoderJson,
        }

        interface LocData {
            lat: number,
            lng: number,
            name: string,
        }

        function getGeocodingData(data: { lat: number, lng: number }): Promise<GeoData> {
            return queryReverseGeocoder(data.lat, data.lng).then((g) => ({ lat: data.lat, lng: data.lng, geo: g }))
        }

        function getLocationData(data: GeoData): Promise<LocData> {
            return Promise.resolve({
                lat: data.lat,
                lng: data.lng,
                name: data.geo.display_name,
            })
        }

        function getContent(data: LocData): model.Content {
            log.info(`success: ${data.lat}, ${data.lng}, '${data.name}'`)
            const html = `<iframe src="streetview.html?lat=${data.lat}&lng=${data.lng}"` +
                ' frameborder="0" tabindex="-1" style="overflow: hidden" width="100%" height="100%"></iframe>'
            return {
                result: 'FOUND',
                content: html,
                solution: data.name,
                detailsUrl: `https://maps.google.com/?ll=${data.lat},${data.lng}`,
            }
        }
    }

    public getDbUriEnvironmentVariableName() {
        return undefined
    }

    public getScraper() {
        return undefined
    }
}
