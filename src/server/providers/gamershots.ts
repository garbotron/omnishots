import * as he from 'he'
import * as model from '../../common/model.js'
import * as common from '../common.js'
import * as db from './gamershots-db.js'

type ListType = 'Blacklist' | 'Whitelist'

interface FilterContext {
    includeReReleases: boolean
}

interface Filter {
    readonly id: string
    getInfo(): Promise<model.FilterInfo>

    // ran for each filter before the query is constructed (2 passes)
    setupContext(context: FilterContext, vals: number[]): void

    // return undefined or match component like { field: { $gte: 42 } }
    getQueryComponent(context: FilterContext, vals: number[]): Promise<any> | undefined
}

class FilterSystem implements Filter {
    public readonly id = 'system'

    public getInfo(): Promise<model.FilterInfo> {
        return db.getAllSystems().then((s) => ({
            id: this.id,
            name: 'Filter by System',
            prompt: '',
            type: model.FilterType.SelectMany,
            names: s,
            defaultValues: [],
        }))
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(context: FilterContext, vals: number[]): Promise<any> | undefined {
        return db.getAllSystems().then((s) => {
            const systems = vals.filter((x) => x >= 0 && x < s.length).map((x) => s[x])
            if (context.includeReReleases) {
                return {
                    $or: [
                        { primarySystems: { $in: systems } },
                        { rereleaseSystems: { $in: systems } },
                    ],
                }
            } else {
                return { primarySystems: { $in: systems } }
            }
        })
    }
}

class FilterIncludeReReleases implements Filter {
    public readonly id = 'include-re-releases'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Include Re-releases?',
            prompt: '',
            type: model.FilterType.SelectOne,
            names: ['Primary Systems Only', 'Include Re-releases'],
            defaultValues: [0],
        })
    }

    public setupContext(context: FilterContext, vals: number[]) {
        context.includeReReleases = vals.length > 0 && vals[0] !== 0
    }

    public getQueryComponent(_: FilterContext, __: number[]): Promise<any> | undefined {
        return undefined
    }
}

class FilterGenre implements Filter {
    public readonly id: string
    private readonly type: ListType

    constructor(type: ListType) {
        this.type = type
        this.id = `genre-${type === 'Blacklist' ? 'black' : 'white'}`
    }

    public getInfo(): Promise<model.FilterInfo> {
        return db.getAllGenres().then((g) => ({
            id: this.id,
            name: `Filter by Genre (${this.type})`,
            prompt: '',
            type: model.FilterType.SelectMany,
            names: g,
            defaultValues: [],
        }))
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        return db.getAllGenres().then((g) => {
            const genres = vals.filter((x) => x >= 0 && x < g.length).map((x) => g[x])
            if (this.type === 'Blacklist') {
                return {
                    $or: [
                        { 'genres.0': { $exists: false } },
                        { genres: { $not: { $in: genres } } },
                    ],
                }
            } else {
                return { genres: { $in: genres } }
            }
        })
    }
}

class FitlerTheme implements Filter {
    public readonly id: string
    private readonly type: ListType

    constructor(type: ListType) {
        this.type = type
        this.id = `theme-${type === 'Blacklist' ? 'black' : 'white'}`
    }

    public getInfo(): Promise<model.FilterInfo> {
        return db.getAllThemes().then((t) => ({
            id: this.id,
            name: `Filter by Theme (${this.type})`,
            prompt: '',
            type: model.FilterType.SelectMany,
            names: t,
            defaultValues: [],
        }))
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        return db.getAllThemes().then((g) => {
            const themes = vals.filter((x) => x >= 0 && x < g.length).map((x) => g[x])
            if (this.type === 'Blacklist') {
                return {
                    $or: [
                        { 'themes.0': { $exists: false } },
                        { themes: { $not: { $in: themes } } },
                    ],
                }
            } else {
                return { themes: { $in: themes } }
            }
        })
    }
}

class FilterReleaseDate implements Filter {
    public readonly id = 'release-date'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Release Date',
            prompt: 'Original release date',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [1950, new Date().getFullYear()],
        })
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        if (vals.length < 2) {
            return undefined
        }
        return Promise.resolve({
            $and: [
                { releaseDate: { $gte: vals[0] } },
                { releaseDate: { $lte: vals[1] } },
            ],
        })
    }
}

class FilterReviewScore implements Filter {
    public readonly id = 'review-score'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Review Score (Out of 100)',
            prompt: 'Average review score',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [0, 100],
        })
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        if (vals.length < 2) {
            return undefined
        }
        return Promise.resolve({
            $and: [
                { averageReviewScore: { $gte: vals[0] } },
                { averageReviewScore: { $lte: vals[1] } },
            ],
        })
    }
}

class FilterNumReviews implements Filter {
    public readonly id = 'num-reviews'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Number of Reviews',
            prompt: 'Minimum number of reviews',
            type: model.FilterType.Number,
            names: [],
            defaultValues: [0],
        })
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        if (!vals.length) {
            return undefined
        }
        return Promise.resolve({ numReviews: { $gte: vals[0] } })
    }
}

class FilterReleaseRegion implements Filter {
    public readonly id = 'release-region'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Release Region',
            prompt: '',
            type: model.FilterType.SelectOne,
            names: ['All Releases', 'US Releases Only'],
            defaultValues: [0],
        })
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        if (vals.length < 1) {
            return undefined
        }
        return Promise.resolve({ regions: { $in: ['United States'] } })
    }
}

const filters: Filter[] = [
    new FilterSystem(),
    new FilterIncludeReReleases(),
    new FilterGenre('Blacklist'),
    new FilterGenre('Whitelist'),
    new FitlerTheme('Blacklist'),
    new FitlerTheme('Whitelist'),
    new FilterReleaseDate(),
    new FilterReviewScore(),
    new FilterNumReviews(),
    new FilterReleaseRegion(),
]

export default class Provider implements common.Provider {
    public readonly id = 'gamershots'

    public getInfo(): Promise<model.ProviderInfo> {
        return Promise.all(filters.map((x) => x.getInfo())).then<model.ProviderInfo>((f) => ({
            id: this.id,
            name: 'Gamershots',
            title: 'Gamershots: Name That Screenshot!',
            prompt: 'What game is this?',
            elemDetailsTitle: 'Game Info',
            description: [
                'Gamershots is a drinking game for gamers.',
                'Check out a screenshot, see if you can name the game!',
                'The concept is simple, but I couldn\'t find another site on the net that does it.',
                '<b>If you can\'t name the game, take a drink!</b>',
                '<b>If you can, everyone else takes a drink and you go again!</b>',
            ],
            filters: f,
        }))
    }

    public checkFilters(filterValues: model.FilterValue[]): Promise<model.FilterTestResult> {
        return this.getQuery(filterValues).then<number>(db.count).then((filteredCount) => {
            return db.count().then((totalCount) => ({ filteredCount, totalCount }))
        })
    }

    public produceContent(filterValues: model.FilterValue[]): Promise<model.Content> {
        return this.getQuery(filterValues).then<db.Game | 'NOT_FOUND'>(db.find).then<model.Content>((x) => {
            if (x === 'NOT_FOUND') {
                return { result: 'NOT_FOUND' }
            } else {
                return {
                    result: 'FOUND',
                    content: this.formatContent(x),
                    solution: this.formatSolution(x),
                    detailsUrl: `https://www.mobygames.com/game/${x.shortName}`,
                }
            }
        })
    }

    public getDbUriEnvironmentVariableName() {
        return db.DB_URI_ENV
    }

    public getScraper(): common.Scraper | undefined {
        return undefined
    }

    private formatContent(g: db.Game): string {
        const screenshots = g.screenshotUrls!
        const i = Math.floor(Math.random() * screenshots.length)
        let url = screenshots[i]
        if (url.startsWith('/')) {
            url = `https://www.mobygames.com/images${url}`
        }
        return `<div style="width:100%;height:100%;display:table;background-image:url(${url});` +
            'background-repeat:no-repeat;background-size:contain;background-position:center;' +
            '-webkit-background-size:contain;-moz-background-size:contain;-o-background-size:contain;"> </div>'
    }

    private formatSolution(g: db.Game): string {
        let systems = g.primarySystems
        if (systems.length > 4) {
            systems = systems.slice(0, 3).concat(['...'])
        }
        return he.encode(`${g.name} (${systems.join(', ')})`)
    }

    private getQuery(filterValues: model.FilterValue[]): Promise<object | undefined> {
        const context: FilterContext = { includeReReleases: false }
        for (const filterValue of filterValues) {
            const filter = filters.find((x) => x.id === filterValue.filterId)
            if (filter) {
                filter.setupContext(context, filterValue.values)
            }
        }

        const matchFilters: Promise<any>[] = []
        for (const filterValue of filterValues) {
            const filter = filters.find((x) => x.id === filterValue.filterId)
            if (filter) {
                const component = filter.getQueryComponent(context, filterValue.values)
                if (component) {
                    matchFilters.push(component)
                }
            }
        }

        if (!matchFilters.length) {
            return Promise.resolve(undefined)
        }
        return Promise.all(matchFilters).then((x) => ({ $and: x }))
    }
}
