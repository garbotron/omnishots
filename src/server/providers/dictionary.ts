import * as model from '../../common/model.js'
import * as common from '../common.js'

const apiKeyEnv = 'OMNISHOTS_DICTIONARY_API_KEY'
const firstYear = 2012
const monthOfFirstYear = 1
const dayOfFirstMonth = 16

export default class Provider implements common.Provider {
    public readonly id = 'dictionary'

    public getInfo(): Promise<model.ProviderInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'DictionaryShots',
            title: 'DictionaryShots: What does it mean?!',
            prompt: 'What does this word mean?',
            elemDetailsTitle: 'Go to the dictionary entry of the word',
            description: [
                'DictionaryShots is a drinking game for vocabulary dorks.',
                'You\'re shown a random English word, see if you know what it means!',
                '<b>If you don\'t, take a drink!</b>',
                '<b>If you do, everyone else takes a drink and you go again!</b>',
            ],
            filters: [],
        })
    }

    public checkFilters(_: model.FilterValue[]): Promise<model.FilterTestResult> {
        const yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000)
        const firstDate = new Date(firstYear, monthOfFirstYear, dayOfFirstMonth)
        const diffMs = yesterday.getTime() - firstDate.getTime()
        const diffDays = Math.floor(diffMs / (24 * 60 * 60 * 1000))
        return Promise.resolve<model.FilterTestResult>({ filteredCount: diffDays, totalCount: diffDays })
    }

    public produceContent(_: model.FilterValue[]): Promise<model.Content> {
        const apiKey = process.env[apiKeyEnv]

        return getWordOfTheDay().then<model.Content>((json) => {
            const data = JSON.parse(json)
            if (!data.word || !data.definitions[0].text) {
                return Promise.reject(new Error('improperly formatted JSON data'))
            }
            const word = data.word as string
            const definition = data.definitions[0].text as string
            return getContent(word, definition)
        })

        function getContent(word: string, definition: string): model.Content {
            const content = '<div style="width:100%;height:100%;display:flex;justify-content:center;">' +
                '<div style="align-self:center;font-size:600%;color:white;">' +
                `<strong>"${word}"</strong>` +
                '</div>'
            return {
                result: 'FOUND',
                content,
                solution: definition.replace(/\.$/, ''),
                detailsUrl: `https://www.wordnik.com/words/${word}`,
            }
        }

        function getRandomDate(): Date {
            // Pick a random day/month/year from the beginning of the word-of-the-day until now (one day ago to be safe)
            const yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000)
            const firstDate = new Date(firstYear, monthOfFirstYear, dayOfFirstMonth)
            return new Date(firstDate.getTime() + Math.random() * (yesterday.getTime() - firstDate.getTime()))
        }

        function getWordOfTheDay(): Promise<string> {
            // Fetch the word-of-the-day JSON for a random date.
            // If this fails due to 404, try again (some of the dates don't have words associated).
            const date = formatDate(getRandomDate(), '-')
            const url = `https://api.wordnik.com/v4/words.json/wordOfTheDay?date=${date}&api_key=${apiKey}`
            return common.get(url).catch((err) => {
                if (err.statusCode === 404) {
                    console.log('Got 404, retrying...')
                    return getWordOfTheDay()
                } else {
                    return Promise.reject(err)
                }
            })
        }

        function formatDate(date: Date, separator: string): string {
            const year = date.getFullYear()
            const month = ('0' + date.getMonth()).slice(-2)
            const day = ('0' + date.getDate()).slice(-2)
            return `${year}${separator}${month}${separator}${day}`
        }
    }

    public getDbUriEnvironmentVariableName() {
        return undefined
    }

    public getScraper() {
        return undefined
    }
}
