import { Cheerio, CheerioAPI, Element, load } from 'cheerio'
import * as he from 'he'
import * as common from '../common.js'
import Fetcher from '../fetcher.js'
import * as db from './animeshots-db.js'

const SKIP_LISTING_SCRAPE = true

interface Context extends common.ScraperContext {
    readonly fetcher: Fetcher
}

// download a webpage via HTTP GET using a proxy and parse it with cheerio
async function fetch(context: Context, url: string): Promise<CheerioAPI> {
    url = rootUrl(url)
    // use part of the "about us" quote at the bottom of the page to test for validity
    return load(await context.fetcher.fetch(url, 'one of the most extensive databases centered on anime'))
}

function rootUrl(page: string): string {
    if (!page.toLowerCase().startsWith('https://')) {
        if (!page.startsWith('/')) {
            page = `/${page}`
        }
        page = `https://www.anisearch.com${page}`
    }
    return page
}

function parseShowListingRow(row: Cheerio<Element>): db.ShowListing {
    const titleLink = row.find('th a')
    const dataCell = row.find('td[data-title=\'Type / Episodes / Year\']')
    const ratingCell = row.find('td[data-title=\'Rating\']')
    if (!titleLink.length || !dataCell.length || !ratingCell.length) {
        throw Error(`Listing table rows not found: ${titleLink} | ${dataCell} | ${ratingCell}: ${row.html()}`)
    }

    const href = titleLink.attr('href')
    if (!href || !href.startsWith('anime/')) {
        throw Error(`Title link was incorrectly formatted: ${titleLink.html()}`)
    }

    // the data cell is formatted "<type>, <num-episodes> <whitespace/newline> (<year>)" where any <> can be "?"
    const dataTextMatch = dataCell.text().match(/^(.+), ([0-9]+|\?)[ \n]+\((.+)\)/)
    if (!dataTextMatch || dataTextMatch.length !== 4) {
        throw Error(`Data cell was incorrectly formatted: ${dataCell.html()}`)
    }

    const [type, numEpisodesStr, yearStr] = dataTextMatch.slice(1)
    const numEpisodes = numEpisodesStr === '?' ? undefined : +numEpisodesStr
    const year = yearStr === '?' ? undefined : +yearStr

    // somewhere inside the rating cell should be a div with class name "star0", "star1" .. "star10" for a 1-10 rating
    const ratingMatch = ratingCell.find('div').toArray()
        .map((x) => x.attribs.class.match(/^star([0-9]+)$/))
        .find((x) => x ? true : false)
    if (!ratingMatch) {
        throw Error(`Rating cell was incorrectly formatted: ${ratingCell.html()}`)
    }
    const rating = ratingMatch[1] === '0' ? undefined : Math.round(+ratingMatch[1] * 10)

    // the show ID is part of the URL: "anime/<show ID>,<show name>"
    const showId = href.substring('anime/'.length, href.indexOf(','))
    const name = he.decode(titleLink.text().trim())

    return { showId, name, type, year, rating, numEpisodes }
}

async function scrapeInfoPage(context: Context, show: db.Show): Promise<void> {
    const url = `https://www.anisearch.com/anime/${show.showId}`
    const $ = await fetch(context, url)
    // collect the average runtime
    const timeTag = $('#information time')
    if (timeTag.length) {
        const timeMatch = timeTag.text().match(/(\d+)\smin/)
        if (!timeMatch || timeMatch.length !== 2) {
            throw new Error(`Time header was incorrectly formatted: ${timeTag.text()}`)
        }
        show.runtimeMinutes = +timeMatch[1]
    }
    // collect the main genres
    $('li > a.gg').each((_, elem) => { show.mainGenres.push(he.decode($(elem).text())) })
    // collect the sub-genres
    $('li > a.gc').each((_, elem) => { show.subGenres.push(he.decode($(elem).text())) })
    // collect the tags
    $('li > a.gt').each((_, elem) => { show.tags.push(he.decode($(elem).text())) })
}

async function scrapeScreenshotsPage(context: Context, show: db.Show): Promise<void> {
    const url = `https://www.anisearch.com/anime/${show.showId}/screenshots`
    const $ = await fetch(context, url)
    for (const ssLink of $('#screenshots a').toArray()) {
        show.screenshotUrls.push(ssLink.attribs.href)
    }
}

// Scrapes a single show listing page. Returns how many shows were scraped.
async function scrapeListingsPage(context: Context, offset: number): Promise<number> {
    context.log.info(`scraping listings page offset=${offset}`)

    const url = `https://www.anisearch.com/anime/index/page-${offset}?limit=100&view=2`
    const $ = await fetch(context, url)

    const listings: db.ShowListing[] = []
    for (const row of common.split($('table.mtC tbody tr'))) {
        listings.push(parseShowListingRow(row))
    }

    const shows = context.localDb.collections[0].entries as db.Show[]
    for (const listing of listings) {
        context.log.info(`found show #${listing.showId}: '${listing.name}'`)
        if (shows.find((x: db.Show) => x.showId === listing.showId)) {
            context.log.info(`skipping show #${listing.showId}: '${listing.name}' since it is already in DB`)
        } else {
            const show: db.Show = {
                showId: listing.showId,
                name: listing.name,
                type: listing.type,
                year: listing.year,
                rating: listing.rating,
                numEpisodes: listing.numEpisodes,
                mainGenres: [],
                subGenres: [],
                tags: [],
                screenshotUrls: [],
            }
            shows.push(show)
        }
    }

    return listings.length
}

async function scrapeAllListingPages(context: Context, offset = 1): Promise<void> {
    if (SKIP_LISTING_SCRAPE) {
        context.log.info("skipping listing page scraping due to SKIP_LISTING_SCRAPE setting")
    } else {
        return scrapeListingsPage(context, offset).then((numShows) => {
            if (numShows < 100) {
                // all done!
                return common.saveLocalDb(context)
            } else {
                return scrapeAllListingPages(context, offset + 1)
            }
        })
    }
}

async function scapeAllIndividualPages(context: Context): Promise<void> {
    const shows = context.localDb.collections[0].entries as db.Show[]
    for (const [idx, show] of shows.entries()) {
        if (show.scrapeDone) {
            context.log.info(`skipping '${show.name}' since the scrape is already done`)
        } else {
            context.log.info(`scraping show #${idx} of ${shows.length}: '${show.name}'`)
            await scrapeInfoPage(context, show)
            await scrapeScreenshotsPage(context, show)
            await common.saveLocalDb(context)
            context.log.info(`  ...complete: ${show.screenshotUrls.length} screenshots`)
            show.scrapeDone = true
        }
    }
}

export default class Scraper implements common.Scraper {
    public async fillLocalDb(context: common.ScraperContext): Promise<void> {
        if (context.localDb.collections.length === 0) {
            context.localDb.collections.push({ name: db.DB_COLLECTION, entries: [] })
        }
        const cxt = { cancelled: false, fetcher: new Fetcher({ timeBetweenRequestsMs: 2500 }), ...context }
        await scrapeAllListingPages(cxt)
        await scapeAllIndividualPages(cxt)
        context.log.info('scrape complete')
    }

    public async prepareLocalDbForUpload(context: common.ScraperContext): Promise<void> {
        // remove all DB entries with no screenshots
        const shows = context.localDb.collections[0].entries as db.Show[]
        let i = 0
        while (i < shows.length) {
            if (shows[i].screenshotUrls.length === 0) {
                context.log.info(`removing '${shows[i].name}' due to no screenshots`)
                shows.splice(i, 1)
            } else {
                ++i
            }
        }

        // remove the 'scrapeDone' flag (no need to upload it)
        for (const show of shows) {
            delete show.scrapeDone
        }
    }
}
