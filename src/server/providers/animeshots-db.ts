import * as mongo from 'mongodb'
import * as common from '../common.js'

export const DB_URI_ENV = 'OMNISHOTS_ANIME_MONGODB_URI'
export const DB_COLLECTION = 'collection'

// Simple type encapsulating just the data that can be scraped from the listing page
export interface ShowListing {
    showId: string
    name: string
    type: string
    year?: number
    rating?: number
    numEpisodes?: number
}

// Full show info
export interface Show extends ShowListing {
    scrapeDone?: boolean
    runtimeMinutes?: number
    mainGenres: string[]
    subGenres: string[]
    tags: string[]
    screenshotUrls: string[]
}

export async function find(query?: mongo.Filter<Show>): Promise<Show | 'NOT_FOUND'> {
    const db = await common.db(DB_URI_ENV, DB_COLLECTION)
    const numRecs = await db.countDocuments(query || {})
    const idx = Math.floor(Math.random() * numRecs)
    const result = await db.find<Show>(query || {}).limit(-1).skip(idx).next()
    return result || 'NOT_FOUND'
}

export async function count(query?: mongo.Filter<Show>): Promise<number> {
    const db = await common.db(DB_URI_ENV, DB_COLLECTION)
    return await db.countDocuments(query ?? {})
}

async function getAllUniqueFromLists(property: string): Promise<string[]> {
    property = '$' + property
    const pipeline = [
        { $unwind: property },
        { $group: { _id: property } },
        { $sort: { _id: 1 } },
    ]
    const shows = await common.db(DB_URI_ENV, DB_COLLECTION)
    const showIds = shows.aggregate<{ _id: string}>(pipeline)
    const showsArr = await showIds.toArray()
    return showsArr.map((x) => x._id)
}

export async function getAllTypes(): Promise<string[]> {
    const pipeline = [
        { $group: { _id: '$type' } },
        { $sort: { _id: 1 } },
    ]
    const shows = await common.db(DB_URI_ENV, DB_COLLECTION)
    const showIds = shows.aggregate<{ _id: string}>(pipeline)
    const showsArr = await showIds.toArray()
    return showsArr.map((x) => x._id)
}

export function getAllMainGenres(): Promise<string[]> {
    return getAllUniqueFromLists('mainGenres')
}

export function getAllSubGenres(): Promise<string[]> {
    return getAllUniqueFromLists('subGenres')
}

export function getAllTags(): Promise<string[]> {
    return getAllUniqueFromLists('tags')
}
