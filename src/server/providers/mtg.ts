import * as he from 'he'
import * as model from '../../common/model.js'
import * as common from '../common.js'

enum CardComponent {
    'Art',
    'Oracle Text',
    'Mana Cost',
    'Type Line',
    'Power/Toughness/Loyalty',
    'Rarity',
    'Set (original)',
    'Flavor Text',
}

enum CardType {
    Land,
    Creature,
    Artifact,
    Enchantment,
    Planeswalker,
    Instant,
    Sorcery,
}

enum Rarity {
    Common,
    Uncommon,
    Rare,
    Mythic,
}

interface CardJson {
    name: string
    scryfall_uri: string
    oracle_text: string
    mana_cost: string
    type_line: string
    power: string | undefined
    toughness: string | undefined
    loyalty: string | undefined
    set_name: string
    flavor_text: string
    image_uris: {
        art_crop: string,
    }
}

interface CardsJson {
    total_cards: number
    has_more: boolean
    data: CardJson[]
}

interface Filter {
    getInfo(): model.FilterInfo
    getFilterComponent(vals: number[]): string | undefined // undefined or query string component
    getCardComponents(vals: number[]): CardComponent[]
}

interface Query {
    urlQuery: string
    cardComponents: CardComponent[]
}

function replaceAll(str: string, find: string, replace: string) {
    return str.replace(new RegExp(find, 'g'), replace)
}

class FilterCardComponents implements Filter {
    public getInfo() {
        // https://stackoverflow.com/questions/18111657/how-does-one-get-the-names-of-typescript-enum-entries
        const components: string[] = []
        for (const x in CardComponent) {
            if (parseInt(x, 10) >= 0) {
                components.push(CardComponent[x])
            }
         }

        return {
            id: 'card-components',
            name: 'Components',
            prompt: 'Which parts of the card to show',
            type: model.FilterType.SelectMany,
            names: components,
            defaultValues: [0],
        }
    }

    public getFilterComponent(vals: number[]) {
        // Make sure at least one of the things we're filtering on is present (for fields that can be missing).
        const parts: string[] = []
        for (const val of vals) {
            switch (val) {
                case CardComponent['Oracle Text']:
                    parts.push('o:/./')
                    break
                case CardComponent['Power/Toughness/Loyalty']:
                    parts.push('pow>-100')
                    parts.push('tou>-100')
                    parts.push('loy>-100')
                    break
                case CardComponent['Flavor Text']:
                    parts.push('ft:/./')
                    break
                default:
                    break
            }
        }

        if (!parts.length) {
            return undefined
        } else if (parts.length === 1) {
            return parts[0]
        } else {
            return `(${parts.join(' or ')})`
        }
    }

    public getCardComponents(vals: number[]) {
        return vals as CardComponent[]
    }
}

class FilterYear implements Filter {
    public getInfo() {
        return {
            id: 'year',
            name: 'Filter by Year',
            prompt: 'Year of first release',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [1993, new Date().getFullYear()],
        }
    }

    public getFilterComponent(vals: number[]) {
        if (vals.length < 2) {
            return undefined // no filter
        }
        return `year>=${vals[0]} year<=${vals[1]}`
    }

    public getCardComponents(_: number[]) {
        return []
    }
}

class FilterCardType implements Filter {
    public getInfo() {
        // https://stackoverflow.com/questions/18111657/how-does-one-get-the-names-of-typescript-enum-entries
        const types: string[] = []
        for (const x in CardType) {
            if (parseInt(x, 10) >= 0) {
                types.push(CardType[x])
            }
        }

        return {
            id: 'Type',
            name: 'Filter by Card Type',
            prompt: 'Card type',
            type: model.FilterType.SelectMany,
            names: types,
            defaultValues: [0],
        }
    }

    public getFilterComponent(vals: number[]) {
        const parts: string[] = []
        for (const val of vals) {
            parts.push(`t:${CardType[val].toLowerCase()}`)
        }

        if (!parts.length) {
            return undefined
        } else if (parts.length === 1) {
            return parts[0]
        } else {
            return `(${parts.join(' or ')})`
        }
    }

    public getCardComponents(_: number[]) {
        return []
    }
}

class FilterRarity implements Filter {
    public getInfo() {
        // https://stackoverflow.com/questions/18111657/how-does-one-get-the-names-of-typescript-enum-entries
        const rarities: string[] = []
        for (const x in Rarity) {
            if (parseInt(x, 10) >= 0) {
                rarities.push(Rarity[x])
            }
        }

        return {
            id: 'rarity',
            name: 'Filter by Rarity',
            prompt: 'Card rarity (any printing)',
            type: model.FilterType.SelectMany,
            names: rarities,
            defaultValues: [0],
        }
    }

    public getFilterComponent(vals: number[]) {
        const parts: string[] = []
        for (const val of vals) {
            parts.push(`in:${Rarity[val].toLowerCase()}`)
        }

        if (!parts.length) {
            return undefined
        } else if (parts.length === 1) {
            return parts[0]
        } else {
            return `(${parts.join(' or ')})`
        }
    }

    public getCardComponents(_: number[]) {
        return []
    }
}

class FilterPrice implements Filter {
    public getInfo() {
        return {
            id: 'price',
            name: 'Filter by Price',
            prompt: 'Current price in USD',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [0.01, 100],
        }
    }

    public getFilterComponent(vals: number[]) {
        if (vals.length < 2) {
            return undefined // no filter
        }
        return `usd>=${vals[0]} usd<=${vals[1]}`
    }

    public getCardComponents(_: number[]) {
        return []
    }
}

class FilterReprints implements Filter {
    public getInfo() {
        return {
            id: 'reprints',
            name: 'Filter Reprints',
            prompt: '',
            type: model.FilterType.SelectOne,
            names: ['Reprints Allowed', 'No Reprints'],
            defaultValues: [0],
        }
    }

    public getFilterComponent(vals: number[]) {
        if (!vals.length || vals[0] === 0) {
            return undefined // default case: reprints allowed
        }
        return 'not:reprint'
    }

    public getCardComponents(_: number[]) {
        return []
    }
}

class FilterBasicLands implements Filter {
    public getInfo() {
        return {
            id: 'basic-lands',
            name: 'Filter Basic Lands',
            prompt: '',
            type: model.FilterType.SelectOne,
            names: ['Basic Lands Included', 'Basic Lands Excluded'],
            defaultValues: [0],
        }
    }

    public getFilterComponent(vals: number[]) {
        if (!vals.length || vals[0] === 0) {
            return undefined // default case: basic lands included
        }
        return '-t:basic'
    }

    public getCardComponents(_: number[]) {
        return []
    }
}


const filters: Filter[] = [
    new FilterCardComponents(),
    new FilterYear(),
    new FilterCardType(),
    new FilterRarity(),
    new FilterPrice(),
    new FilterReprints(),
    new FilterBasicLands(),
]

export default class Provider implements common.Provider {
    public readonly id = 'mtg'

    public getInfo(): Promise<model.ProviderInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Magic the Gathering',
            title: 'MtG Guessing Game',
            prompt: 'What card is this?',
            elemDetailsTitle: 'Card Info',
            description: [
                'The MtG guessing game is a drinking game for Magic fans.',
                'Do you know the card from just the art? The oracle text?',
                '<b>If you can\'t get it, take a drink!</b>',
                '<b>If you can, everyone else takes a drink and you go again!</b>',
            ],
            filters: filters.map((x) => x.getInfo()),
        })
    }

    public checkFilters(filterValues: model.FilterValue[]): Promise<model.FilterTestResult> {
        const filteredQuery = this.getQuery(filterValues)
        const totalQuery = this.getQuery([])
        return this.performScryfallFullSearch(filteredQuery.urlQuery).then((filteredJson) => {
            return this.performScryfallFullSearch(totalQuery.urlQuery).then((totalJson) => {
                return { filteredCount: filteredJson.total_cards, totalCount: totalJson.total_cards }
            })
        })
    }

    public produceContent(filterValues: model.FilterValue[]): Promise<model.Content> {
        const query = this.getQuery(filterValues)
        return this.performScryfallRandomSearch(query.urlQuery).then((json) => {
            return this.formatContent(json, query.cardComponents)
        })
    }

    public getScraper() {
        return undefined
    }

    public getDbUriEnvironmentVariableName() {
        return undefined
    }

    private formatContent(json: CardJson, components: CardComponent[]): model.Content {
        if (!json) {
            return { result: 'NOT_FOUND' }
        }

        return {
            result: 'FOUND',
            content: this.renderContent(json, components),
            solution: `${json.name} (${json.set_name})`,
            detailsUrl: json.scryfall_uri,
        }
    }

    private renderContent(json: CardJson, components: CardComponent[]): string {
        function hasComponent(c: CardComponent) {
            // Default to "everything included".
            return !components.length || components.includes(c)
        }

        let outerStyle = 'width:100%;height:100%;display:flex;text-align:center;justify-content:center;'
        if (hasComponent(CardComponent.Art)) {
            outerStyle += `background-image:url(${json.image_uris.art_crop});`
                + 'background-repeat:no-repeat;background-size:contain;background-position:center;'
                + '-webkit-background-size:contain;-moz-background-size:contain;-o-background-size:contain;'
        }

        const innerStyle = 'align-self:center;margin:5em;color:aliceblue;font-size:250%;line-height:1.25em;'
            + 'text-shadow:1px 1px 2px #111,1px -1px 2px #111,-1px  1px 2px #111,-1px -1px 2px #111;'
            + 'font-weight:bold;'

        const content: string[] = []

        if (hasComponent(CardComponent['Mana Cost']) && json.mana_cost) {
            content.push(`Mana cost: ${he.encode(json.mana_cost)}`)
        }

        if (hasComponent(CardComponent['Type Line']) && json.type_line) {
            content.push(`Type: ${he.encode(json.type_line)}`)
        }

        if (hasComponent(CardComponent['Oracle Text']) && json.oracle_text) {
            content.push(`${he.encode(replaceAll(json.oracle_text, json.name, '~'))}`)
        }

        if (hasComponent(CardComponent['Power/Toughness/Loyalty'])) {
            if (json.power || json.toughness) {
                content.push(`${json.power || '?'}/${json.toughness || '?'}`)
            }
            if (json.loyalty) {
                content.push(`${json.loyalty} loyalty`)
            }
        }

        if (hasComponent(CardComponent['Flavor Text']) && json.flavor_text) {
            content.push(`<em>${he.encode(json.flavor_text)}</em>`)
        }

        if (hasComponent(CardComponent['Set (original)']) && json.set_name) {
            content.push(`(${he.encode(json.set_name)})`)
        }

        const formattedContent = content.map((x) => x.replace(/(?:\r\n|\r|\n)/g, '<br>')).join('<br>')
        return `<div style="${outerStyle}"><div style="${innerStyle}">${formattedContent || ' '}</div></div>`
    }

    private performScryfallFullSearch(query: string): Promise<CardsJson> {
        return common.getJson<CardsJson>(`https://api.scryfall.com/cards/search?q=${encodeURIComponent(query)}`)
    }

    private performScryfallRandomSearch(query: string): Promise<CardJson> {
        return common.getJson<CardJson>(`https://api.scryfall.com/cards/random?q=${encodeURIComponent(query)}`)
    }

    private getQuery(filterValues: model.FilterValue[]): Query {
        const terms: string[] = []
        const components: CardComponent[] = []

        for (const filterValue of filterValues) {
            const filter = filters.find((x) => x.getInfo().id === filterValue.filterId)
            if (filter) {
                const term = filter.getFilterComponent(filterValue.values)
                if (term) {
                    terms.push(term)
                }
                filter.getCardComponents(filterValue.values).forEach((x) => components.push(x))
            }
        }

        if (!terms.length) {
            terms.push('year>1900')
        }

        terms.push('unique:art')

        return {
            urlQuery: terms.join(' '),
            cardComponents: components,
        }
    }
}
