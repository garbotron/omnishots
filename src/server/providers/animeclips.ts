import * as model from '../../common/model.js'
import * as common from '../common.js'

// This server should be running the anime clip host (https://gitlab.com/garbotron/omnishots-anime-clip-host)
const HttpServer = 'https://animeops.giddeongarber.info:8117'

interface FilterContext {
    clipLengthSecs: number
}

interface Filter {
    readonly id: string
    getInfo(): Promise<model.FilterInfo>
    setupContext(context: FilterContext, vals: number[]): void
}

class FilterClipLength implements Filter {
    public readonly id = 'clip-length'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Clip Length',
            prompt: 'Clip length in seconds',
            type: model.FilterType.Number,
            names: [],
            defaultValues: [6],
        })
    }

    public setupContext(context: FilterContext, vals: number[]) {
        if (vals.length > 0) {
            context.clipLengthSecs = vals[0]
        }
    }
}

const filters: Filter[] = [
    new FilterClipLength(),
]

export default class Provider implements common.Provider {
    public readonly id = 'animeclips'

    public getInfo(): Promise<model.ProviderInfo> {
        return Promise.all(filters.map((x) => x.getInfo())).then<model.ProviderInfo>((f) => ({
            id: this.id,
            name: 'Anime Openings',
            title: 'Animeclips: Name That Show!',
            prompt: 'What show is this?',
            elemDetailsTitle: 'Full opening',
            description: [
                'Animeclips is a drinking game for anime dorks.',
                'Check out a bit of an opening, see if you can name the show!',
                '<b>If you can\'t name the show, take a drink!</b>',
                '<b>If you can, everyone else takes a drink and you go again!</b>',
            ],
            filters: f,
        }))
    }

    public checkFilters(_: model.FilterValue[]): Promise<model.FilterTestResult> {
        return common.get(`${HttpServer}/count`).then((x) => ({ filteredCount: +x, totalCount: +x }))
    }

    public produceContent(filterValues: model.FilterValue[]): Promise<model.Content> {
        const context: FilterContext = { clipLengthSecs: 6 }
        for (const filterValue of filterValues) {
            const filter = filters.find((x) => x.id === filterValue.filterId)
            if (filter) {
                filter.setupContext(context, filterValue.values)
            }
        }
        return common.get(`${HttpServer}/random`).then((x) => this.formatContent(x, context.clipLengthSecs))
    }

    public getScraper() {
        return undefined
    }

    public getDbUriEnvironmentVariableName() {
        return undefined
    }

    private formatContent(clipName: string, clipLengthSecs: number): model.Content {
        const clipFile = `${encodeURIComponent(clipName)}.webm`
        return {
            result: 'FOUND',
            content: `<video poster='video-loading.gif' style='width: 100%; height: 100%;' controls preload autoplay>`
                + `<source src='${HttpServer}/clip/${clipLengthSecs}/${clipFile}' type='video/mp4'>`
                + `</video>`,
            solution: clipName,
            detailsUrl: `${HttpServer}/full/${clipFile}`,
        }
    }
}
