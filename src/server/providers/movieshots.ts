import * as model from '../../common/model.js'
import * as common from '../common.js'

export const apiKeyEnv = 'OMNISHOTS_MOVIES_API_KEY'
const maxPages = 1000
const approxTotalMovieCount = 540000

type ListType = 'Blacklist' | 'Whitelist'

interface Filter {
    readonly id: string
    getInfo(): Promise<model.FilterInfo>
    getQueryComponent(vals: number[]): Promise<string[]>
}

interface JsonGenres {
    genres: {
        id: number,
        name: string,
    }[]
}

interface JsonDiscoverMovie {
    id: number,
    title: string,
    release_date: string,
}

interface JsonDiscover {
    total_results: number,
    total_pages: number,
    results: JsonDiscoverMovie[]
}

interface JsonConfiguration {
    images: {
        secure_base_url: string,
    }
}

interface JsonImages {
    backdrops: {
        file_path: string,
    }[]
}

function queryMovieDb<TJson>(subUri: string, args: string[]) {
    const baseUri = 'https://api.themoviedb.org/3'
    const apiKey = process.env[apiKeyEnv]
    if (!apiKey) {
        throw new Error(`${apiKeyEnv} not set`)
    }
    const fullUri = `${baseUri}/${subUri}?api_key=${apiKey}&${args.join('&')}`
    return common.getJson<TJson>(fullUri)
}

class FilterDate implements Filter {
    public readonly id = 'date'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Date',
            prompt: 'Original release date',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [1800, new Date().getFullYear()],
        })
    }

    public getQueryComponent(vals: number[]): Promise<string[]> {
        if (vals.length < 2) {
            return Promise.resolve([])
        }
        return Promise.resolve([
            `primary_release_date.gte=${vals[0]}-01-01`,
            `primary_release_date.lte=${vals[1]}-12-31`,
        ])
    }
}

class FilterGenre implements Filter {
    public readonly id: string
    private readonly type: ListType
    private jsonCache: JsonGenres | undefined = undefined

    constructor(type: ListType) {
        this.type = type
        this.id = `genre-${type === 'Blacklist' ? 'black' : 'white'}`
    }

    public getInfo(): Promise<model.FilterInfo> {
        return this.queryIfNeeded().then((s) => ({
            id: this.id,
            name: `Filter by Genre (${this.type})`,
            prompt: '',
            type: model.FilterType.SelectMany,
            names: s.genres.map((x) => x.name),
            defaultValues: [],
        }))
    }

    public getQueryComponent(vals: number[]): Promise<string[]> {
        return this.queryIfNeeded().then((s) => {
            const genreNames = s.genres.map((x) => x.name)
            const genreIds: string[] = []
            for (const val of vals.filter((x) => x >= 0 && x < genreNames.length)) {
                const matchingGenre = s.genres.find((x) => x.name === genreNames[val])
                if (matchingGenre) {
                    genreIds.push(`${matchingGenre.id}`)
                }
            }
            if (genreIds.length === 0) {
                return []
            }
            const paramName = this.type === 'Blacklist' ? 'without_genres' : 'with_genres'
            return [`${paramName}=${genreIds.join('|')}`]
        })
    }

    private queryIfNeeded(): Promise<JsonGenres> {
        if (this.jsonCache) {
            return Promise.resolve(this.jsonCache)
        } else {
            return queryMovieDb<JsonGenres>('genre/movie/list', ['language=en-US']).then((x) => {
                this.jsonCache = x
                return x
            })
        }
    }
}

class FilterVoteCount implements Filter {
    public readonly id = 'vote-count'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Vote Count',
            prompt: 'Minimum vote count',
            type: model.FilterType.Number,
            names: [],
            defaultValues: [50],
        })
    }

    public getQueryComponent(vals: number[]): Promise<string[]> {
        if (vals.length === 0) {
            return Promise.resolve([])
        }
        return Promise.resolve([`vote_count.gte=${vals[0]}`])
    }
}

class FilterVoteAverage implements Filter {
    public readonly id = 'vote-average'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Average Rating (Out of 100)',
            prompt: 'Average voted rating',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [0, 100],
        })
    }

    public getQueryComponent(vals: number[]): Promise<string[]> {
        if (vals.length === 0) {
            return Promise.resolve([])
        }
        return Promise.resolve([`vote_average.gte=${vals[0] / 10}`])
    }
}

class FilterReleaseRegion implements Filter {
    public readonly id = 'release-region'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Release Region',
            prompt: '',
            type: model.FilterType.SelectOne,
            names: ['All Releases', 'US Releases Only'],
            defaultValues: [0],
        })
    }

    public getQueryComponent(vals: number[]): Promise<string[]> {
        if (vals.length === 0 || vals[0] === 0) {
            return Promise.resolve([])
        }
        return Promise.resolve(['region=US'])
    }
}

class FilterReleaseType implements Filter {
    public readonly id = 'release-type'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Release Type',
            prompt: '',
            type: model.FilterType.SelectOne,
            names: ['All Releases', 'Theatrical Releases Only'],
            defaultValues: [0],
        })
    }

    public getQueryComponent(vals: number[]): Promise<string[]> {
        if (vals.length === 0 || vals[0] === 0) {
            return Promise.resolve([])
        }
        return Promise.resolve(['with_release_type=3'])
    }
}

class FilterLanguage implements Filter {
    public readonly id = 'language'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Original Language',
            prompt: '',
            type: model.FilterType.SelectOne,
            names: ['All Languages', 'English Only'],
            defaultValues: [0],
        })
    }

    public getQueryComponent(vals: number[]): Promise<string[]> {
        if (vals.length === 0 || vals[0] === 0) {
            return Promise.resolve([])
        }
        return Promise.resolve(['with_original_language=en'])
    }
}

class FilterRuntime implements Filter {
    public readonly id = 'runtime'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Average Runtime',
            prompt: 'Average runtime (minutes)',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [5, 300],
        })
    }

    public getQueryComponent(vals: number[]): Promise<string[]> {
        if (vals.length < 2) {
            return Promise.resolve([])
        }
        return Promise.resolve([
            `with_runtime.gte=${vals[0]}`,
            `with_runtime.lte=${vals[1]}`,
        ])
    }
}

const filters: Filter[] = [
    new FilterDate(),
    new FilterGenre('Blacklist'),
    new FilterGenre('Whitelist'),
    new FilterVoteCount(),
    new FilterVoteAverage(),
    new FilterReleaseRegion(),
    new FilterReleaseType(),
    new FilterLanguage(),
    new FilterRuntime(),
]

export default class Provider implements common.Provider {
    public readonly id = 'movieshots'

    public getInfo(): Promise<model.ProviderInfo> {
        return Promise.all(filters.map((x) => x.getInfo())).then<model.ProviderInfo>((f) => ({
            id: this.id,
            name: 'Movieshots',
            title: 'Movieshots: Name That Screenshot!',
            prompt: 'What movie is this?',
            elemDetailsTitle: 'Movie Info',
            description: [
                'Movieshots is a drinking game for movie nerds.',
                'Check out a screenshot, see if you can name the movie!',
                '<b>If you can\'t name the movie, take a drink!</b>',
                '<b>If you can, everyone else takes a drink and you go again!</b>',
            ],
            filters: f,
        }))
    }

    public checkFilters(filterValues: model.FilterValue[]): Promise<model.FilterTestResult> {
        return this.getQuery(filterValues).then((x) => {
            return queryMovieDb<JsonDiscover>('discover/movie', x)
        }).then((x) => {
            return { filteredCount: x.total_results, totalCount: approxTotalMovieCount }
        })
    }

    public produceContent(filterValues: model.FilterValue[]): Promise<model.Content> {
        let query: string[]
        let jsonMovie: JsonDiscoverMovie
        let jsonCfg: JsonConfiguration

        return this.getQuery(filterValues).then((x) => {
            query = x
            return queryMovieDb<JsonDiscover>('discover/movie', query)
        }).then((movies) => {
            if (!movies.total_results) {
                return this.createNotFoundContent()
            }

            const pageIdx = Math.floor(Math.random() * Math.min(maxPages, movies.total_pages))
            const pageNum = pageIdx + 1
            query.push(`page=${pageNum}`)

            return queryMovieDb<JsonDiscover>('discover/movie', query).then((properPage) => {
                const movieIdx = Math.floor(Math.random() * properPage.results.length)
                jsonMovie = properPage.results[movieIdx]
                return queryMovieDb<JsonConfiguration>('configuration', [])
            }).then((cfg) => {
                jsonCfg = cfg
                return queryMovieDb<JsonImages>(`movie/${jsonMovie.id}/images`, [])
            }).then((imgs) => {
                if (!imgs.backdrops.length) {
                    return this.createStandardContent(jsonMovie, undefined)
                }

                const imgIdx = Math.floor(Math.random() * imgs.backdrops.length)
                const subPath = imgs.backdrops[imgIdx].file_path
                const imgUrl = `${jsonCfg.images.secure_base_url}w1280${subPath}`
                return this.createStandardContent(jsonMovie, imgUrl)
            })
        })
    }

    public getScraper() {
        return undefined
    }

    public getDbUriEnvironmentVariableName() {
        return undefined
    }

    private createNotFoundContent(): model.Content {
        return { result: 'NOT_FOUND' }
    }

    private createStandardContent(json: JsonDiscoverMovie, imgUrl: string | undefined): model.Content {
        let year = json.release_date
        const hyphenIdx = year.indexOf('-')
        if (hyphenIdx >= 0) {
            year = year.substr(0, hyphenIdx)
        }

        let outerStyle = 'width:100%;height:100%;display:flex;text-align:center;justify-content:center;'
        if (imgUrl) {
            outerStyle += `background-image:url(${imgUrl});`
                + 'background-repeat:no-repeat;background-size:contain;background-position:center;'
                + '-webkit-background-size:contain;-moz-background-size:contain;-o-background-size:contain;'
        }

        const innerStyle = 'align-self:center;margin:5em;color:aliceblue;font-size:250%;line-height:1.25em;'
            + 'text-shadow:1px 1px 2px #111,1px -1px 2px #111,-1px  1px 2px #111,-1px -1px 2px #111;'
            + 'font-weight:bold;'

        const text = imgUrl ? ' ' : 'No screenshots for this movie (try adding a votes filter)'
        const content = `<div style="${outerStyle}"><div style="${innerStyle}">${text}</div></div>`

        return {
            result: 'FOUND',
            content,
            solution: `${json.title} (${year})`,
            detailsUrl: `https://www.themoviedb.org/movie/${json.id}`,
        }
    }

    private getQuery(filterValues: model.FilterValue[]): Promise<string[]> {
        const terms: Promise<string[]>[] = []
        terms.push(Promise.resolve(['language=en-US']))
        for (const filterValue of filterValues) {
            const filter = filters.find((x) => x.id === filterValue.filterId)
            if (filter) {
                const component = filter.getQueryComponent(filterValue.values)
                if (component) {
                    terms.push(component)
                }
            }
        }
        // Flatten the array-of-arrays into a single flat array.
        return Promise.all(terms).then((x) => x.reduce((acc, val) => acc.concat(val), []))
    }
}
