// Scraper for the Jeopardy! provider, loosely based on the J!Party crawler (https://github.com/j-party/crawler)

import { Cheerio, CheerioAPI, Element, load } from 'cheerio'
import * as he from 'he'
import * as common from '../common.js'
import Fetcher from '../fetcher.js'
import * as db from './jeopardy-db.js'

interface Context extends common.ScraperContext {
    readonly fetcher: Fetcher
}

// download a webpage via HTTP GET (rate limited) and parse it with cheerio
async function fetch(context: Context, url: string): Promise<CheerioAPI> {
    url = rootUrl(url)
    return load(await context.fetcher.fetch(url, 'j-a.gif'))
}

function rootUrl(page: string): string {
    if (!page.toLowerCase().startsWith('https://')) {
        if (!page.startsWith('/')) {
            page = `/${page}`
        }
        page = `https://j-archive.com${page}`
    }
    return page
}

// split a cheerio match into separate submatches using eq()
function split<T>($: Cheerio<T>): Cheerio<T>[] {
    const ret: Cheerio<T>[] = []
    for (let i = 0; i < $.length; i++) {
        ret.push($.eq(i))
    }
    return ret
}

function get_clue_prefix(idx: number): string {
    switch (idx) {
        case 0: return 'clue_J'
        case 1: return 'clue_DJ'
        case 2: return 'clue_TJ'
        default: throw new Error(`Unexpected board index: ${idx}`)
    }
}

async function scrapeEpisode(context: Context, url: string): Promise<void> {
    const $ = await fetch(context, url)
    return getShowInfo($)

    function getShowInfo($: CheerioAPI) {
        context.log.info(`Scraping episode: ${url}`)
        const year = $('#game_title').text().match(/[0-9]{4}$/)
        if (!year) {
            throw new Error(`Couldn't parse year: ${url}`)
        }

        let questions: db.Question[] = []
        for (const [idx, round] of split($('.round > tbody')).entries()) {
            questions = questions.concat(getQuestionsFromBoard(round, get_clue_prefix(idx), +year))
        }
        const finalRound = $('.final_round')
        if (finalRound.length > 0) {
            questions.push(getFinalQuestion($('.final_round'), +year))
        }
        context.localDb.collections[0].entries.push(...questions)
    }

    function getQuestionsFromBoard(board: Cheerio<Element>, cluePrefix: string, year: number): db.Question[] {
        // loop through the categories, adding the questions & answers
        const questions: db.Question[] = []
        for (let col = 1; col <= 6; col++) {
            const category = he.decode(board.find('td.category').eq(col - 1).find('.category_name').text())
            for (let row = 1; row <= 5; row++) {
                const cellId = `#${cluePrefix}_${col}_${row}`
                const clue = he.decode(board.find(cellId).text())
                const answer = he.decode(board.find(`${cellId}_r .correct_response`).text())
                // on rare occasion, the clue boxes are emtpy - we need to check for this condition
                if (clue && answer) {
                    questions.push({
                        year,
                        category,
                        level: row,
                        clue,
                        answer,
                        url: rootUrl(url),
                    })
                }
            }
        }
        return questions
    }

    function getFinalQuestion(board: Cheerio<Element>, year: number): db.Question {
        return {
            year,
            category: board.find('.category_name').text(),
            level: 'FINAL',
            clue: he.decode(board.find('#clue_FJ').text()),
            answer: he.decode(board.find('#clue_FJ_r .correct_response').text()),
            url: rootUrl(url),
        }
    }
}

async function scrapeSeason(context: Context, url: string): Promise<void> {
    context.log.info(`Scraping season: ${url}`)
    const $ = await fetch(context, url)
    const hrefs = split($('#content table a')).map((x) => x.attr('href'))
    for (const url of hrefs.filter((x) => x!.search(/(^|\/)showgame\.php/) > -1)) {
        await scrapeEpisode(context, url!)
    }
}

async function scrapeSeasonList(context: Context, url: string): Promise<void> {
    context.log.info(`Scraping seasons: ${url}`)
    const $ = await fetch(context, url)
    const hrefs = split($('#content table a')).map((x) => x.attr('href'))
    for (const url of hrefs.filter((x) => x!.search(/(^|\/)showseason\.php/) > -1)) {
        await scrapeSeason(context, url!)
    }
}

async function scrape(context: Context): Promise<void> {
    await scrapeSeasonList(context, 'listseasons.php')
    return context.log.info('scrape complete')
}

export default class Scraper implements common.Scraper {
    public fillLocalDb(context: common.ScraperContext): Promise<void> {
        context.localDb.collections = [{ name: db.DB_COLLECTION, entries: [] }]
        return scrape({
            fetcher: new Fetcher({ timeBetweenRequestsMs: 1000 }),
            ...context
        })
    }

    public async prepareLocalDbForUpload(_: common.ScraperContext): Promise<void> {
        // nothing needed
    }
}
