import * as he from 'he'
import * as model from '../../common/model.js'
import * as common from '../common.js'
import * as db from './animeshots-db.js'
import Scraper from './animeshots-scraper.js'

type ListType = 'Blacklist' | 'Whitelist'

interface FilterContext {
    includeReReleases: boolean
}

interface Filter {
    readonly id: string
    getInfo(): Promise<model.FilterInfo>

    // ran for each filter before the query is constructed (2 passes)
    setupContext(context: FilterContext, vals: number[]): void

    // return undefined or match component like { field: { $gte: 42 } }
    getQueryComponent(context: FilterContext, vals: number[]): Promise<any> | undefined
}

class FilterType implements Filter {
    public readonly id = 'type'

    public getInfo(): Promise<model.FilterInfo> {
        return db.getAllTypes().then((s) => ({
            id: this.id,
            name: 'Filter by Type',
            prompt: '',
            type: model.FilterType.SelectMany,
            names: s,
            defaultValues: [],
        }))
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        return db.getAllTypes().then((s) => {
            const types = vals.filter((x) => x >= 0 && x < s.length).map((x) => s[x])
            return { type: { $in: types } }
        })
    }
}

class FilterDate implements Filter {
    public readonly id = 'date'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Date',
            prompt: 'Original release date',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [1900, new Date().getFullYear()],
        })
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        if (vals.length < 2) {
            return undefined
        }
        return Promise.resolve({
            $and: [
                { year: { $exists: true } },
                { year: { $gte: vals[0] } },
                { year: { $lte: vals[1] } },
            ],
        })
    }
}

class FilterMainGenre implements Filter {
    public readonly id: string
    private readonly type: ListType

    constructor(type: ListType) {
        this.type = type
        this.id = `main-genre-${type === 'Blacklist' ? 'black' : 'white'}`
    }

    public getInfo(): Promise<model.FilterInfo> {
        return db.getAllMainGenres().then((t) => ({
            id: this.id,
            name: `Filter by Main Genre (${this.type})`,
            prompt: '',
            type: model.FilterType.SelectMany,
            names: t,
            defaultValues: [],
        }))
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        return db.getAllMainGenres().then((g) => {
            const genres = vals.filter((x) => x >= 0 && x < g.length).map((x) => g[x])
            if (this.type === 'Blacklist') {
                return {
                    $or: [
                        { 'mainGenres.0': { $exists: false } },
                        { mainGenres: { $not: { $in: genres } } },
                    ],
                }
            } else {
                return { mainGenres: { $in: genres } }
            }
        })
    }
}

class FilterSubGenre implements Filter {
    public readonly id: string
    private readonly type: ListType

    constructor(type: ListType) {
        this.type = type
        this.id = `sub-genre-${type === 'Blacklist' ? 'black' : 'white'}`
    }

    public getInfo(): Promise<model.FilterInfo> {
        return db.getAllSubGenres().then((t) => ({
            id: this.id,
            name: `Filter by Sub-Genre (${this.type})`,
            prompt: '',
            type: model.FilterType.SelectMany,
            names: t,
            defaultValues: [],
        }))
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        return db.getAllSubGenres().then((g) => {
            const genres = vals.filter((x) => x >= 0 && x < g.length).map((x) => g[x])
            if (this.type === 'Blacklist') {
                return {
                    $or: [
                        { 'subGenres.0': { $exists: false } },
                        { subGenres: { $not: { $in: genres } } },
                    ],
                }
            } else {
                return { subGenres: { $in: genres } }
            }
        })
    }
}

class FilterTag implements Filter {
    public readonly id: string
    private readonly type: ListType

    constructor(type: ListType) {
        this.type = type
        this.id = `tag-${type === 'Blacklist' ? 'black' : 'white'}`
    }

    public getInfo(): Promise<model.FilterInfo> {
        return db.getAllTags().then((t) => ({
            id: this.id,
            name: `Filter by Tag (${this.type})`,
            prompt: '',
            type: model.FilterType.SelectMany,
            names: t,
            defaultValues: [],
        }))
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        return db.getAllTags().then((g) => {
            const tags = vals.filter((x) => x >= 0 && x < g.length).map((x) => g[x])
            if (this.type === 'Blacklist') {
                return {
                    $or: [
                        { 'tags.0': { $exists: false } },
                        { tags: { $not: { $in: tags } } },
                    ],
                }
            } else {
                return { tags: { $in: tags } }
            }
        })
    }
}

class FilterRating implements Filter {
    public readonly id = 'rating'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Average Rating (Out of 100)',
            prompt: 'Average rating',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [0, 100],
        })
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        if (vals.length < 2) {
            return undefined
        }
        return Promise.resolve({
            $and: [
                { rating: { $exists: true } },
                { rating: { $gte: vals[0] } },
                { rating: { $lte: vals[1] } },
            ],
        })
    }
}

class FilterNumEpisodes implements Filter {
    public readonly id = 'num-episodes'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Number of Episodes',
            prompt: 'Number of episodes',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [1, 52],
        })
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): Promise<any> | undefined {
        if (vals.length < 2) {
            return undefined
        }
        return Promise.resolve({
            $and: [
                { numEpisodes: { $exists: true } },
                { numEpisodes: { $gte: vals[0] } },
                { numEpisodes: { $lte: vals[1] } },
            ],
        })
    }
}

class FilterRuntime implements Filter {
    public readonly id = 'runtime'

    public getInfo(): Promise<model.FilterInfo> {
        return Promise.resolve({
            id: this.id,
            name: 'Filter by Average Runtime',
            prompt: 'Average runtime (minutes)',
            type: model.FilterType.NumberRange,
            names: [],
            defaultValues: [5, 300],
        })
    }

    public setupContext(_: FilterContext, __: number[]) {
        // do nothing
    }

    public getQueryComponent(_: FilterContext, vals: number[]): any {
        if (vals.length < 2) {
            return undefined
        }
        return Promise.resolve({
            $and: [
                { runtimeMinutes: { $exists: true } },
                { runtimeMinutes: { $gte: vals[0] } },
                { runtimeMinutes: { $lte: vals[1] } },
            ],
        })
    }
}

const filters: Filter[] = [
    new FilterType(),
    new FilterDate(),
    new FilterMainGenre('Blacklist'),
    new FilterMainGenre('Whitelist'),
    new FilterSubGenre('Blacklist'),
    new FilterSubGenre('Whitelist'),
    new FilterTag('Blacklist'),
    new FilterTag('Whitelist'),
    new FilterRating(),
    new FilterNumEpisodes(),
    new FilterRuntime(),
]

export default class Provider implements common.Provider {
    public readonly id = 'animeshots'

    public getInfo(): Promise<model.ProviderInfo> {
        return Promise.all(filters.map((x) => x.getInfo())).then<model.ProviderInfo>((f) => ({
            id: this.id,
            name: 'Animeshots',
            title: 'Animeshots: Name That Screenshot!',
            prompt: 'What show is this?',
            elemDetailsTitle: 'Show Info',
            description: [
                'Animeshots is a drinking game for anime geeks.',
                'Check out a screenshot, see if you can name the show!',
                'The concept is simple, but I couldn\'t find another site on the net that does it.',
                '<b>If you can\'t name the show, take a drink!</b>',
                '<b>If you can, everyone else takes a drink and you go again!</b>',
            ],
            filters: f,
        }))
    }

    public checkFilters(filterValues: model.FilterValue[]): Promise<model.FilterTestResult> {
        return this.getQuery(filterValues).then<number>(db.count).then((filteredCount) => {
            return db.count().then((totalCount) => ({ filteredCount, totalCount }))
        })
    }

    public produceContent(filterValues: model.FilterValue[]): Promise<model.Content> {
        return this.getQuery(filterValues).then<db.Show | 'NOT_FOUND'>(db.find).then<model.Content>((x) => {
            if (x === 'NOT_FOUND') {
                return { result: 'NOT_FOUND' }
            } else {
                return {
                    result: 'FOUND',
                    content: this.formatContent(x),
                    solution: this.formatSolution(x),
                    detailsUrl: `https://www.anisearch.com/anime/${x.showId}`,
                }
            }
        })
    }

    public getDbUriEnvironmentVariableName() {
        return db.DB_URI_ENV
    }

    public getScraper(): common.Scraper | undefined {
        return new Scraper()
    }

    private formatContent(s: db.Show): string {
        const i = Math.floor(Math.random() * s.screenshotUrls.length)
        return `<div style="width:100%;height:100%;display:table;background-image:url(${s.screenshotUrls[i]});` +
            'background-repeat:no-repeat;background-size:contain;background-position:center;' +
            '-webkit-background-size:contain;-moz-background-size:contain;-o-background-size:contain;"> </div>'
    }

    private formatSolution(s: db.Show): string {
        return he.encode(`${s.name} (${s.year})`)
    }

    private getQuery(filterValues: model.FilterValue[]): Promise<object | undefined> {
        const context: FilterContext = { includeReReleases: false }
        for (const filterValue of filterValues) {
            const filter = filters.find((x) => x.id === filterValue.filterId)
            if (filter) {
                filter.setupContext(context, filterValue.values)
            }
        }

        const matchFilters: Promise<any>[] = []
        for (const filterValue of filterValues) {
            const filter = filters.find((x) => x.id === filterValue.filterId)
            if (filter) {
                const component = filter.getQueryComponent(context, filterValue.values)
                if (component) {
                    matchFilters.push(component)
                }
            }
        }

        if (!matchFilters.length) {
            return Promise.resolve(undefined)
        }
        return Promise.all(matchFilters).then((x) => ({ $and: x }))
    }
}
