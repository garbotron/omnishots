import { Cheerio } from 'cheerio'
import { gotScraping, ToughCookieJar, OptionsInit } from 'got-scraping'
import * as fs from 'node:fs'
import * as path from 'path'
import * as lambda from 'aws-lambda'
import * as mongo from 'mongodb'
import * as model from '../common/model.js'
import * as logger from './logger.js'

export function squash<T>(array: T[][]): T[] {
    return array.reduce((a, b) => a.concat(b))
}

export interface LocalDb {
    collections: Array<{ name: string, entries: any[] }>
}

export interface ScraperContext {
    readonly log: logger.Logger
    readonly localDbPath: string
    localDb: LocalDb
}

export interface Scraper {
    fillLocalDb(context: ScraperContext): Promise<void>
    prepareLocalDbForUpload(context: ScraperContext): Promise<void>
}

export interface Provider {
    readonly id: string
    getInfo(): Promise<model.ProviderInfo>
    checkFilters(fv: model.FilterValue[]): Promise<model.FilterTestResult>
    produceContent(fv: model.FilterValue[]): Promise<model.Content>
    getDbUriEnvironmentVariableName(): string | undefined
    getScraper(): Scraper | undefined
}

interface HttpOptions {
    timeout?: number
    cookies?: ToughCookieJar
    username?: string
    password?: string
    authBearer?: string
    form?: object
}

export type Handler<TInput, TOutput> = (input: TInput) => Promise<TOutput>

// We're using a separate database per provider (to squeeze the most out of free cloud DB services).
const dbConnections = new Map<string, { client: mongo.MongoClient, db: mongo.Db }>()

export function saveLocalDb(context: ScraperContext): Promise<void> {
    const data = JSON.stringify(context.localDb)
    return fs.promises.writeFile(context.localDbPath, data, 'utf8')
}

function getRequestOptions(opts?: HttpOptions): OptionsInit | undefined {
    if (!opts) {
        return undefined
    }
    const config: OptionsInit = {}
    config.maxRedirects = 5
    if (opts.timeout) {
        config.timeout = { request: opts.timeout }
    }
    if (opts.cookies) {
        config.cookieJar = opts.cookies
    }
    if (opts.username) {
        config.username = opts.username
    }
    if (opts.password) {
        config.password = opts.password
    }
    if (opts.authBearer) {
        config.headers = {
            authorization: `Bearer ${opts.authBearer}`
        }
    }
    if (opts.form) {
        config.form = opts.form
    }
    return config
}

export function get(uri: string, opts?: HttpOptions): Promise<string> {
    // Use Got rather than default nodejs since it handles redirects.
    const result = gotScraping.get(uri, getRequestOptions(opts))
    if (result instanceof Promise) {
        return result.text()
    } else {
        throw Error("result wasn't a promise")
    }
}

export async function getJson<TJson>(uri: string, opts?: HttpOptions): Promise<TJson> {
    const text = await get(uri, opts)
    return JSON.parse(text)
}

export async function post<TJson>(uri: string, opts?: HttpOptions): Promise<TJson> {
    const result = gotScraping.post(uri, getRequestOptions(opts));
    if (result instanceof Promise) {
        const text = await result.text()
        return JSON.parse(text)
    } else {
        throw Error("result wasn't a promise")
    }
}

// split a cheerio match into separate submatches using eq()
export function split<T>($: Cheerio<T>): Cheerio<T>[] {
    const ret: Cheerio<T>[] = []
    for (let i = 0; i < $.length; i++) {
        ret.push($.eq(i))
    }
    return ret
}

async function dbTop(dbUriEnvironmentVariableName: string): Promise<mongo.Db> {
    // This doesn't need to be a promise right now, but we'll keep this API in case we want it to be a promise later...
    const connection = dbConnections.get(dbUriEnvironmentVariableName)
    if (connection) {
        return connection.db
    } else {
        throw Error('DB not initialized')
    }
}

export async function openDb(dbUriEnvironmentVariableName: string): Promise<void> {
    const dbUri = process.env[dbUriEnvironmentVariableName]
    if (!dbUri) {
        throw new Error(`${dbUriEnvironmentVariableName} not set`)
    }

    const options = {
        heartbeatFrequencyMS: 10000,
        connectTimeoutMS: 30000,
    }

    const x = await mongo.MongoClient.connect(dbUri, options)
    dbConnections.set(dbUriEnvironmentVariableName, { client: x, db: x.db() })
}

// Get a handle to the specified DB collection
export async function db(dbUriEnvironmentVariableName: string, collectionName: string): Promise<mongo.Collection> {
    const db = await dbTop(dbUriEnvironmentVariableName)
    return db.collection(collectionName)
}

// Push a local DB (in-memory structure) to the MongoDB store
export async function pushLocalDb(dbUriEnvironmentVariableName: string, localDb: LocalDb): Promise<void> {
    const db = await dbTop(dbUriEnvironmentVariableName)

    const curCollectionsCursor = await db.listCollections()
    const curCollections = await curCollectionsCursor.toArray()
    for (const c of curCollections) {
        await db.dropCollection(c.name)
    }

    for (const col of localDb.collections) {
        const newCollection = await db.createCollection(col.name)
        await newCollection.insertMany(col.entries)
    }
}

export async function closeDbs(): Promise<void> {
    const entry = dbConnections.entries().next()
    if (!entry.done) {
        const [key, val] = entry.value
        await val.client.close()
        dbConnections.delete(key)
        return await closeDbs()
    }
}

export function localDbFilePath(provider_id: string): string {
    return path.join(path.dirname(__filename), `db-${provider_id}.json`)
}

export async function run<TInput, TOutput>(
    name: string,
    event: lambda.APIGatewayProxyEvent,
    handler: Handler<TInput, TOutput>): Promise<lambda.APIGatewayProxyResult> {

    console.log(`function invoked: ${name}`)

    const headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
    }

    // treat invalid requests or requests without a 'body' parameter as having an empty body
    const body = event.body || '{}'
    const input = JSON.parse(body) as TInput

    return handler(input)
        .then(async (result) => {
            await closeDbs()
            console.log(`${name}: success`)
            return {
                statusCode: 200,
                headers,
                body: JSON.stringify(result),
            }
        })
        .catch(async (e) => {
            await closeDbs()
            if (e.code && e.message) {
                console.error(`${name}: error ${e.code}: ${e.message}`)
                console.error(JSON.stringify(e))
                return {
                    statusCode: e.code,
                    headers,
                    body: e.message,
                }
            }
            else {
                console.error(e)
                return {
                    statusCode: 400,
                    headers,
                    body: 'Unexpected error!',
                }
            }
        })
}
