# [omnishots](https://garbotron.gitlab.io/omnishots/)

Engine for web-based drinking games, written with [AWS CDK](https://aws.amazon.com/cdk/) and [TypeScript](https://www.typescriptlang.org/).

[![build status](https://gitlab.com/garbotron/omnishots/badges/master/pipeline.svg)](https://gitlab.com/garbotron/omnishots/pipelines)

# TODO

Remember to include here:
- How to run scraper (npm run scraper -- target)
- How to build
- How to deploy

# Architecture Notes

- Games are managed purely on the client
- Game state is stored in local storage

# Functions:

- get-providers:
  - Input: {}
  - Output: {
      ProviderReport
  }
- get-new-question:
  - Input: {
      provider //short name
      filterValues //FilterValue[]
  }
  - Output: {
      content //string
      solution //string
      detailsUrl //string
  }
